import { RouteObject } from "react-router-dom";
import {
  AdminLayout,
  AuthLayout,
  DetailLayout,
  MainLayout,
} from "components/layouts";
import {
  AdminJob,
  AdminJobType,
  AdminService,
  AdminUser,
  Home,
  JobDetail,
  Login,
  Profile,
  Register,
} from "pages";
import { PATH } from "constant";
import { DetailTemplate } from "components/templates";
import Result from "pages/Result";
export const router: RouteObject[] = [
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Home />,
      },
    ],
  },
  {
    path: "/",
    element: <AuthLayout />,
    children: [
      {
        path: PATH.login,
        element: <Login />,
      },
      {
        path: PATH.register,
        element: <Register />,
      },
    ],
  },
  {
    path: PATH.detail,
    element: <DetailLayout />,
    children: [
      {
        index: true,
        element: <DetailTemplate />,
      },
    ],
  },
  {
    path: PATH.result,
    element: <DetailLayout />,
    children: [
      {
        index: true,
        element: <Result />,
      },
    ],
  },
  {
    path: PATH.jobDetail,
    element: <DetailLayout />,
    children: [
      {
        index: true,
        element: <JobDetail />,
      },
    ],
  },
  {
    path: PATH.profile,
    element: <DetailLayout />,
    children: [
      {
        index: true,
        element: <Profile />,
      },
    ],
  },
  {
    path: PATH.admin,
    element: <AdminLayout />,
    children: [
      {
        index: true,
        path: PATH.adminUser,
        element: <AdminUser />,
      },
      {
        path: PATH.adminJob,
        element: <AdminJob />,
      },
      {
        path: PATH.adminjobType,
        element: <AdminJobType />,
      },
      {
        path: PATH.adminService,
        element: <AdminService />,
      },
    ],
  },
];
