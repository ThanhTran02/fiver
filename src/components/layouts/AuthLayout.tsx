import { Footer, Header, Navbar } from "components/ui";
import { Outlet } from "react-router-dom";
import { useAppDispatch } from "store";
import { quanLyAuthActions } from "store/quanLyAuth/slice";
import { styled } from "styled-components";

export const AuthLayout = () => {
  const dispatch = useAppDispatch();
  dispatch(quanLyAuthActions.changeScroll(101));
  return (
    <Container className="relative ">
      <div className="fixed top-0 left-0 z-50 w-full bg-white border-[#e4e5e7]  ">
        <div className="border-b-[1px] w-full ">
          <Header />
        </div>
        <div className="border-[#e4e5e7] w-full  border-b-[1px]">
          <Navbar />
        </div>
      </div>
      <div className="pt-[150px] items-center flex justify-center  bg-cover object-contain body">
        <Outlet />
      </div>
      <div className="w-full  border-t-[1px] border-[#e4e5e7]">
        <Footer />
      </div>
    </Container>
  );
};

export default AuthLayout;
const Container = styled.div`
  margin: auto;
  .body {
    max-width: 1280px;
    margin: auto;
  }
`;
