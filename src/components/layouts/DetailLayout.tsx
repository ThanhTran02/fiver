import { Outlet } from "react-router-dom";
import { styled } from "styled-components";
import { useEffect } from "react";

import { Footer, Header, Navbar } from "components/ui";
import { quanLyAuthActions } from "store/quanLyAuth/slice";
import { useAppDispatch } from "store";
import { getMenuLoaiCVThunk } from "store/QuanLyCongViec/thunk";

export const DetailLayout = () => {
  const dispatch = useAppDispatch();
  dispatch(quanLyAuthActions.changeScroll(101));
  useEffect(() => {
    dispatch(getMenuLoaiCVThunk());
  });
  return (
    <Container className="relative ">
      <div className="fixed top-0 left-0 z-50 w-full bg-white border-[#e4e5e7]  ">
        <div className="border-b-[1px] w-full ">
          <Header />
        </div>
        <div className="border-[#e4e5e7] w-full  border-b-[1px]">
          <Navbar />
        </div>
      </div>
      <div className="pt-[150px] items-center flex justify-center  bg-cover object-contain body z-0">
        <Outlet />
      </div>
      <div className="  border-t-[1px] bg-white border-[#e4e5e7] z-10">
        <Footer />
      </div>
    </Container>
  );
};

export default DetailLayout;
const Container = styled.div`
  margin: auto;
  .body {
    max-width: 1280px;
    margin: auto;
  }
`;
