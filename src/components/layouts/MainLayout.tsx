import { Outlet } from "react-router-dom";
import { styled } from "styled-components";
import { Banner, Container, Footer, Header, Navbar } from "../ui";
import { useState, useEffect } from "react";
import { useAppDispatch } from "store";
import { quanLyAuthActions } from "store/quanLyAuth/slice";
export const MainLayout = () => {
  const [scrollPosition, setScrollPosition] = useState(0);
  const dispatch = useAppDispatch();
  useEffect(() => {
    const handleScroll = () => {
      const position = window.screenY || document.documentElement.scrollTop;
      setScrollPosition(position);
      dispatch(quanLyAuthActions.changeScroll(position));
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <ContainerS className="relative m-0 ">
      <div className="fixed top-0 left-0 z-20 w-full bg-transparent   ">
        <div
          className={
            scrollPosition > 100 ? `border-b-[1px] w-full text-white ` : ``
          }
        >
          <Header />
        </div>
        <div
          className={
            scrollPosition > 100
              ? `border-[#e4e5e7] w-full border-b-[1px] bg-white`
              : "hidden"
          }
        >
          <Navbar />
        </div>
      </div>
      <div className="banner relatives top-0 left-0 z-10 ">
        <Banner />
        <Container />
      </div>
      <div className="pt-[60px] items-center flex justify-center  bg-cover object-contain ">
        <Outlet />
      </div>
      <div className="footer border-t-[1px] border-[#e4e5">
        <Footer />
      </div>
    </ContainerS>
  );
};

export default MainLayout;
const ContainerS = styled.div`
  .banner {
    padding: 0;
    margin: auto;
    height: 100vh !important;
  }
`;
