import { Outlet, useNavigate } from "react-router-dom";
import { useState } from "react";
import { styled } from "styled-components";

import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  SettingOutlined,
  ContainerOutlined,
  ContactsOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { AiFillHome } from "react-icons/ai";
import { Layout, Menu, Button, theme, MenuProps } from "antd";

import { Popover } from "components/ui";
import { PATH } from "constant";

const { Header, Sider, Content } = Layout;
export const AdminLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const [current, setCurrent] = useState("1");
  const navigate = useNavigate();
  const onClick: MenuProps["onClick"] = (e) => {
    switch (e.key) {
      case "1":
        navigate(PATH.adminUser);
        break;
      case "2":
        navigate(PATH.adminJob);
        break;
      case "3":
        navigate(PATH.adminjobType);
        break;
      case "4":
        navigate(PATH.adminService);
        break;
    }
    setCurrent(e.key);
  };
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Container>
      <Layout className="">
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div
            className={`text-white px-2 py-2 text-center font-bold font-mono text-2xl ${
              collapsed ? "hidden" : ""
            }`}
          >
            Dashboard
          </div>
          <Menu
            onClick={onClick}
            theme="dark"
            mode="inline"
            selectedKeys={[current]}
            items={[
              {
                key: "1",
                icon: <UserOutlined />,
                label: "Manage User",
              },
              {
                key: "2",
                icon: <ContactsOutlined />,
                label: "Manage Job",
              },
              {
                key: "3",
                icon: <ContainerOutlined />,
                label: "Manage JobType",
              },
              {
                key: "4",
                icon: <SettingOutlined />,
                label: "Manage Service",
              },
            ]}
          />
        </Sider>
        <Layout>
          <Header style={{ padding: 0, background: colorBgContainer }}>
            <div className="flex justify-between mr-10 items-center cursor-pointer">
              <Button
                type="text"
                icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                onClick={() => setCollapsed(!collapsed)}
                style={{
                  fontSize: "16px",
                  width: 64,
                  height: 64,
                }}
              />
              <Popover
                content={
                  <div>
                    <h2 className="font-semibold p-2">Quản trị viên</h2>
                    <hr />
                    <div
                      className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
                      onClick={() => navigate("/")}
                    >
                      Quay về trang chủ
                    </div>
                  </div>
                }
              >
                <AiFillHome className="w-10 h-10" />
              </Popover>
            </div>
          </Header>
          <Content
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
              background: colorBgContainer,
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </Container>
  );
};

export default AdminLayout;
const Container = styled.section`
  margin: auto;
  height: 100vh;
  .body {
    max-width: 1280px;
    margin: auto;
  }
`;
