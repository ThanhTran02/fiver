import { AvatarIcon, LockIcon } from "components/icon";
import { zodResolver } from "@hookform/resolvers/zod";
import { Input, InputPassword } from "components/ui";
import { styled } from "styled-components";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-toastify";

import { LoginSchema, LoginSchemaType } from "schema";
import { PATH } from "constant";
import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "store";
import { loginThunk } from "store/quanLyAuth/thunk";
import { getUserInfoThunk } from "store/quanLyNguoiDung/thunk";

export const LoginTemplate = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });

  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
    dispatch(loginThunk(value))
      .unwrap()
      .then(() => {
        dispatch(getUserInfoThunk());
        navigate("/");
      })
      .catch((error) => {
        toast.error(error.response.data.content);
      });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container className="grid grid-cols-2">
        <div>
          <img
            src="https://demo5.cybersoft.edu.vn/static/media/signin.6f1c72291c1ec0817ded.jpg"
            alt=""
          />
        </div>
        <div className="flex flex-col gap-10">
          <h2 className="text-[#1dbf73] font-bold text-4xl">
            Sign In to Fiverr
          </h2>
          <div>
            <div className="flex items-center text-[rgba(0,0,0,.85)]">
              <AvatarIcon h={18} />
              <Input
                placeholder="Your Email"
                name="email"
                error={errors?.email?.message}
                register={register}
              />
            </div>
            <div className="flex items-center text-[rgba(0,0,0,.85)] mt-4">
              <LockIcon h={18} />
              <InputPassword
                placeholder="Your Password"
                register={register}
                name="password"
                error={errors?.password?.message}
              />
            </div>
          </div>
          <div className=" flex gap-4">
            <button
              type="submit"
              className="rounded-full bg-[#1dbf73] text-white py-1 px-6 border-0 btn-login"
            >
              LOGIN
            </button>
            <p
              className="rounded-full text-[#1dbf73] bg-white border-[1px] border-[#1dbf73]  hover:border-[#1dbf73] py-2 px-6 cursor-pointer"
              onClick={() => navigate(PATH.register)}
            >
              Register now?
            </p>
          </div>
        </div>
      </Container>
    </form>
  );
};

export default LoginTemplate;
const Container = styled.div`
  .btn-login {
    transition: 1s;
    border: none;
    &:hover {
      background: #188652 !important;
      box-shadow: 2px 2px 4px 2px #d3d6d4;
      transform: translateY(-8px);
    }
  }
  .ant-input-search-button {
    background-color: #1dbf73;

    &:active {
      background-color: #19a463 !important;
    }
    &:hover {
      background-color: #19a463 !important;
    }
  }
  .ant-input {
    /* background-color: #00b96b; */
    &:hover {
      border-color: #19a463;
    }
    &:focus {
      border-color: #19a463;
    }
  }
  .css-dev-only-do-not-override-18iikkb.ant-input-affix-wrapper {
    border-color: #19a463 !important;
  }
`;
