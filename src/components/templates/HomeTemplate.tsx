import { useEffect, useRef, useState } from "react";
import { BiCheckCircle } from "react-icons/bi";
import { styled } from "styled-components";

import { Modal, SubCarousel, Testimonial } from "components/ui";
import { getMenuLoaiCVThunk } from "store/QuanLyCongViec/thunk";
import { useAppDispatch } from "store";

export const HomeTemplate = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const videoRef = useRef<HTMLVideoElement | null>(null);
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    if (videoRef.current) {
      videoRef.current.pause();
    }
  };
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getMenuLoaiCVThunk());
  }, [dispatch]);
  return (
    <ContainerS className="mt-10">
      <div className="content">
        <h2 className="text-4xl font-semibold  mb-10">
          Popular professional services
        </h2>
        <SubCarousel />
      </div>

      <div className="bg-[#f0fdf6] py-20 mt-10">
        <div className="grid grid-cols-2 gap-4 content">
          <div>
            <h2 className="text-4xl font-semibold  mb-6">
              A whole world of freelance talent at your fingertips
            </h2>
            <div>
              <div className="flex  items-center gap-2">
                <BiCheckCircle className="fill-[rgb(122, 125, 133)] text-xl" />
                <h3 className="text-xl text-black font-bold">
                  The best for every budget
                </h3>
              </div>
              <p className="mt-2">
                Find high-quality services at every price point. No hourly
                rates, just project-based pricing.
              </p>
            </div>
            <div>
              <div className="flex  items-center gap-2">
                <BiCheckCircle className="fill-[rgb(122, 125, 133)] text-xl" />
                <h3 className="text-xl text-black font-bold">
                  Protected payments, every time
                </h3>
              </div>
              <p className="mt-2">
                Always know what you'll pay upfront. Your payment isn't released
                until you approve the work.
              </p>
            </div>
            <div>
              <div className="flex  items-center gap-2">
                <BiCheckCircle className="fill-[rgb(122, 125, 133)] text-xl" />
                <h3 className="text-xl text-black font-bold">24/7 support</h3>
              </div>
              <p className="mt-2">
                Questions? Our round-the-clock support team is available to help
                anytime, anywhere.
              </p>
            </div>
          </div>
          <div className="cursor-pointer video-img" onClick={showModal}>
            <img src="https://demo5.cybersoft.edu.vn/img/selling.png" alt="" />
          </div>
          <Modal
            open={isModalOpen}
            footer
            onCancel={handleCancel}
            className="!w-3/4"
          >
            <video className="aspect-video w-full p-3" controls ref={videoRef}>
              <source src="./video/sell.mp4" type="video/mp4" />
            </video>
          </Modal>
        </div>
      </div>
      <div className="content">
        <Testimonial />
      </div>
      <div className="content py-16">
        <h2 className="text-4xl font-semibold  mb-10">
          Explore the marketplace
          <ul className="grid grid-cols-6">
            <li className=" text-center mt-5">
              <a
                href="#"
                className=" flex items-center  justify-center flex-col"
              >
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/graphics-design.d32a2f8.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/online-marketing.74e221b.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/writing-translation.32ebe2e.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/video-animation.f0d9d71.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/music-audio.320af20.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
          </ul>
          <ul className="grid-cols-3 grid w-1/2 m-auto">
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/business.bbdf319.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lifestyle.745b575.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
            <li className="mt-5 text-center">
              <a href="#" className=" flex items-center flex-col">
                <img
                  className="main-categories-img"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/data.718910f.svg"
                  alt="Graphics &amp; Design"
                  loading="lazy"
                />
                <span>Graphics &amp; Design</span>
              </a>
            </li>
          </ul>
        </h2>
      </div>
    </ContainerS>
  );
};

export default HomeTemplate;

const ContainerS = styled.section`
  width: 100vw;
  .content {
    max-width: 1180px;
    margin: auto;
  }

  .video-img {
    z-index: 0;
    position: relative;
    &::after {
      background: url(https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/desktop-play-button.c1196d6.png)
        no-repeat;
      background-size: 80px;
      content: "";
      display: inline-block;
      height: 80px;
      left: 50%;
      position: absolute;
      top: 40%;
      -webkit-transform: translateX(-50%);
      transform: translateX(-50%);
      width: 80px;
      z-index: 1;
    }
  }
  .testimonial-logo {
    border-left: 1px solid #c5c6c9;
    background-color: transparent;
    margin: 10px 15px;
    padding: 0 10px;
  }
  .main-categories-img {
    height: 70px;
    width: 70px;
    margin: auto;
  }
  a {
    color: #222325;
    font-size: 16px;
    display: inline-block;
    position: relative;
    text-align: center;
    text-decoration: none;
    &::after {
      border-bottom: 2px solid #c5c6c9;
      border-color: 0.2s ease-in-out, padding 0.2s ease-in-out !important;
      content: "";
      padding: 0 0 8px;
      position: absolute;
      right: 50%;
      top: 65px;
      -webkit-transform: translateX(50%);
      transform: translateX(50%);
      width: 48px;
      transition: 0.5s;
    }
  }
  a {
    &:hover {
      &::after {
        border-color: #1dbf73;
        border-color: 0.2s ease-in-out, padding 0.2s ease-in-out !important;
        width: 60%;
      }
    }
  }
  img {
    cursor: pointer;
  }
`;
