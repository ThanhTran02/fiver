import { styled } from "styled-components";

import { MyJob, MyProfile } from "components/ui";

export const ProfileTemplate = () => {
  return (
    <Container className="grid grid-cols-3 w-full">
      <div className="col-span-1">
        <MyProfile />
      </div>
      <div className="col-span-2">
        <MyJob />
      </div>
    </Container>
  );
};

export default ProfileTemplate;
const Container = styled.div``;
