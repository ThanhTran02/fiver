import { styled } from "styled-components";
import { Switch } from "antd";
import { Card, Select } from "components/ui";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate, useParams } from "react-router-dom";

import { RootState, useAppDispatch } from "store";
import { getCVTheoTenThunk } from "store/QuanLyCongViec/thunk";
import { DetailCongViec } from "types";
import { HeartIcon, SaoICon } from "components/icon";
import { PATH } from "constant";

export const ResultTemplate = () => {
  const params = useParams();
  const { name } = params;

  const { arrCV } = useSelector((state: RootState) => state.quanLyCongViec);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  const onChange = (checked: boolean) => {
    console.log(`switch to ${checked}`);
  };

  useEffect(() => {
    if (name) dispatch(getCVTheoTenThunk(name));
  }, [name]);

  return (
    <Container className="mb-10">
      <div className="flex items-center justify-start gap-5">
        <Select
          defaultValue="Categogy"
          style={{ width: 200 }}
          onChange={handleChange}
          options={[
            { value: "all", label: "All Categories" },
            { value: "Web", label: "Web Programing  (20,566)}" },
            { value: "Data", label: "Data Entry (12,566)" },
          ]}
        />
        <Select
          defaultValue="Service Options"
          style={{ width: 200 }}
          onChange={handleChange}
          options={[
            { value: "all", label: "All Categories" },
            { value: "Web", label: "Web Programing  (20,566)}" },
            { value: "Data", label: "Data Entry (12,566)" },
          ]}
        />{" "}
        <Select
          defaultValue="Seller Details"
          style={{ width: 200 }}
          onChange={handleChange}
          options={[
            { value: "all", label: "All Categories" },
            { value: "Web", label: "Web Programing  (20,566)}" },
            { value: "Data", label: "Data Entry (12,566)" },
          ]}
        />{" "}
        <Select
          defaultValue="Delivery Time"
          style={{ width: 200 }}
          onChange={handleChange}
          options={[
            { value: "all", label: "All Categories" },
            { value: "Web", label: "Web Programing  (20,566)}" },
            { value: "Data", label: "Data Entry (12,566)" },
          ]}
        />
      </div>
      <div className="my-4 flex gap-5">
        <div className="flex gap-2 items-center">
          <Switch defaultChecked onChange={onChange} />
          <p className="text-[#62646a] font-semibold  text-xl">Pro services</p>
        </div>
        <div className="flex gap-2 items-center">
          <Switch onChange={onChange} />
          <p className="text-[#62646a] font-semibold text-xl">Local sellers</p>
        </div>
        <div className="flex gap-2 items-center">
          <Switch onChange={onChange} />
          <p className="text-[#62646a] font-semibold  text-xl">
            Online sellers
          </p>
        </div>
      </div>
      <div className="mb-4 flex justify-between">
        <div>
          <p className="text-[#62646a] font-semibold">
            {arrCV?.length} <span className="">services available</span>
          </p>
        </div>
        <div className="flex gap-2">
          <p className="text-[#62646a] font-semibold">Sort by</p>
          <Select
            defaultValue="Relevance"
            style={{ width: 150 }}
            onChange={handleChange}
            options={[
              { value: 0, label: "Relevance" },
              { value: 1, label: "Best Selling}" },
              { value: 2, label: "New Arrivals" },
            ]}
          />{" "}
        </div>
      </div>
      <div className="grid grid-cols-4 gap-5 ">
        {arrCV?.map((item: DetailCongViec) => (
          <div key={item.id}>
            <Card
              style={{
                height: 450,
                display: "flex",
                flexDirection: "column",
                justifyContent: "start",
              }}
              cover={<img alt="..." src={item.congViec.hinhAnh} />}
            >
              <Card.Meta
                avatar={
                  <img
                    alt="...."
                    src={item.avatar}
                    className="h-6 w-6 rounded-full"
                  />
                }
                title={
                  <div>
                    <h4>{item.tenNguoiTao}</h4>
                    <p className="text-[#74767e] text-[16px]">
                      Level {item.congViec.saoCongViec} Seller
                    </p>
                  </div>
                }
              />
              <div
                className="mt-4 font-semibold h-24 text-[16px] flex flex-col justify-between cursor-pointer"
                onClick={() => {
                  const path = generatePath(PATH.jobDetail, { idJob: item.id });
                  navigate(path);
                }}
              >
                <p>{item.congViec.tenCongViec}</p>
                <span className=" flex gap-2 items-center">
                  <SaoICon />
                  <p className="text-[#74767e]">
                    (<span>{item.congViec.danhGia}</span>)
                  </p>
                </span>
              </div>
              <Card.Meta
                title={
                  <div className="flex justify-between items-center mt-4 py-4 border-t-[1px] border-[rgba(0, 0, 0, 0.175)] ">
                    <div className="fill-[#74767e]">
                      <HeartIcon />
                    </div>
                    <div className="">
                      <p className="m-0 text-[#74767e] font-semibold">
                        Starting at{" "}
                        <span className="font-bold textt-xl text-[#404145]">
                          US${item.congViec.giaTien}
                        </span>
                      </p>
                    </div>
                  </div>
                }
              />
            </Card>
          </div>
        ))}
      </div>
    </Container>
  );
};

export default ResultTemplate;
const Container = styled.div`
  .ant-switch-checked {
    background: #1dbf73 !important;
  }
  .ant-select-selection-item {
    font-weight: bold !important;
    color: #1dbf73 !important;
    font-size: 16px !important;
  }
`;
