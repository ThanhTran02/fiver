import { useSelector } from "react-redux";
import { useEffect } from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { AiFillEdit, AiOutlineDelete } from "react-icons/ai";
import { toast } from "react-toastify";
import { isAxiosError } from "axios";
import { Modal, RadioChangeEvent } from "antd";
import { styled } from "styled-components";
import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import { useState } from "react";

import { Button, Input, InputAnt, Pagination, Radio } from "components/ui";
import { RootState, useAppDispatch } from "store";
import { getAllJobHiredThunk } from "store/quanlyAdmin/thunk";
import { JobHiredSchema, JobHiredSchemaType } from "schema";
import { quanLyAdminActions } from "store/quanlyAdmin/slice";

import { quanLyThueCongViecService } from "services";
import { deleteCVHiredCVThunk } from "store/quanLyThueCongViec/thunk";

export const AdminServiceTemplate = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [genderValue, setGenderValue] = useState(false);
  const [isModalAddJobHiredOpen, setIsModalAddJobHiredOpen] = useState(false);
  const [displayedProducts, setDisplayedProducts] = useState(true);
  const [chooseID, setChooseID] = useState();
  const { currentPage } = useSelector((state: RootState) => state.quanLyAdmin);
  const dispatch = useAppDispatch();

  const handlePageChange = (page: number) => {
    dispatch(quanLyAdminActions.setCurrentPage(page));
  };

  const onChange = (e: RadioChangeEvent) => {
    setGenderValue(e.target.value);
  };
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<JobHiredSchemaType>({
    mode: "onChange",
    resolver: zodResolver(JobHiredSchema),
  });

  const showModal = (type: string) => {
    type === "add" ? setIsModalAddJobHiredOpen(true) : setIsModalOpen(true);
  };

  const onSubmitAddJob: SubmitHandler<any> = async (value) => {
    try {
      await quanLyThueCongViecService.addThueCVAdmin(value);
      setIsModalAddJobHiredOpen(false);
      toast.success("Đăng ký thành công");
    } catch (error) {
      if (isAxiosError<{ content: string }>(error)) {
        toast.error(error.response?.data.content);
      }
    }
    setDisplayedProducts(!displayedProducts);
  };

  const onSubmit: SubmitHandler<JobHiredSchemaType> = async (value) => {
    try {
      await quanLyThueCongViecService.updateThueCVAdmin({
        id: chooseID,
        value: value,
      });
      setIsModalOpen(false);
      toast.success("Cập nhật thành công");
    } catch (err) {
      console.log(err);
    }

    setDisplayedProducts(!displayedProducts);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsModalAddJobHiredOpen(false);
  };

  const confirm = (id: number) => {
    Modal.confirm({
      title: "Do you Want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      onOk() {
        dispatch(deleteCVHiredCVThunk(id))
          .unwrap()
          .then(() => {
            toast.success("Xoá thành công ");
            setDisplayedProducts(!displayedProducts);
          });
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const handleSearch = (event: any) => {
    dispatch(getAllJobHiredThunk({ search: event.target.value, page: 1 }));
  };

  const { allServiceHire } = useSelector(
    (state: RootState) => state.quanLyAdmin
  );
  useEffect(() => {
    dispatch(getAllJobHiredThunk({ search: "", page: currentPage }));
  }, [displayedProducts, currentPage]);
  return (
    <div className="flex flex-col gap-5">
      <Button
        className="bg-transparent border-2 w-40 border-y-indigo-300 "
        onClick={() => showModal("add")}
      >
        Add new
      </Button>
      <InputAnt
        placeholder="Tìm kiếm thông tin người dùng"
        onChange={handleSearch}
      ></InputAnt>
      <table className="w-[700px]! text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Id</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>
            <th scope="col" className="px-6 py-3">
              JobID
            </th>
            <th scope="col" className="px-6 py-3">
              Hirer ID
            </th>
            <th scope="col" className="px-6 py-3">
              Condition
            </th>
            <th scope="col" className="px-6 py-3">
              Day
            </th>
            <th scope="col" className="px-6 py-3">
              Action
            </th>
          </tr>
        </thead>
        <tbody>
          {allServiceHire &&
            allServiceHire.map((job: any, index: number) => (
              <tr
                className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
                key={index}
              >
                <th
                  scope="row"
                  className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                >
                  {job.id}
                </th>
                <td>{job.maCongViec}</td>
                <td>{job.maNguoiThue}</td>
                <td>
                  {job.hoanThanh === false
                    ? "Chưa hoàn thành"
                    : "Đã hoàn thành"}
                </td>
                <td>{job.ngayThue}</td>

                <td>
                  <div className=" items-center flex justify-center ">
                    <button
                      className="bg-transparent"
                      onClick={() => {
                        setChooseID(job.id);
                        reset(job);
                        showModal("");
                      }}
                    >
                      <AiFillEdit className="text-2xl " />
                    </button>
                    <button
                      className="ml-8 bg-transparent"
                      onClick={() => confirm(job.id)}
                    >
                      <AiOutlineDelete className="text-2xl " />
                    </button>
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
      <div className="w-full  flex justify-center">
        <Pagination
          defaultCurrent={1}
          onChange={handlePageChange}
          total={500}
          className=" !my-2"
        />
      </div>

      <Modal
        open={isModalOpen}
        onCancel={handleCancel}
        footer
        width={425}
        centered
        title={<h2 className="text-center font-semibold">Update profile</h2>}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex items-center flex-col justify-center">
            <Input
              label="Mã Công việc"
              register={register}
              name="maCongViec"
              placeholder="Mã Công việc"
              error={errors?.maCongViec?.message}
            />
            <Input
              label="Mã người thuê"
              register={register}
              name="maNguoiThue"
              placeholder="Mã người thuê"
              error={errors?.maNguoiThue?.message}
            />
            <Input
              label="Ngày thuê"
              register={register}
              name="ngayThue"
              placeholder="Ngày Thuê"
              error={errors?.ngayThue?.message}
            />

            <Radio.Group onChange={onChange} value={genderValue}>
              <Radio value={false}>Chưa hoành thành</Radio>
              <Radio value={true}>Đã hoàn thành</Radio>
            </Radio.Group>
            <ButtonS type="submit">ADD</ButtonS>
          </div>
        </form>
      </Modal>
      <Modal
        open={isModalAddJobHiredOpen}
        onCancel={handleCancel}
        footer
        width={450}
        centered
        title={<h2 className="text-center font-semibold">Add new jobType</h2>}
      >
        <form
          className=" w-full flex flex-col items-center "
          onSubmit={handleSubmit(onSubmitAddJob)}
        >
          <div className="flex items-center flex-col justify-center">
            <Input
              label="Mã Công việc"
              register={register}
              name="maCongViec"
              placeholder="Mã Công việc"
              error={errors?.maCongViec?.message}
            />
            <Input
              label="Mã người thuê"
              register={register}
              name="maNguoiThue"
              placeholder="Mã người thuê"
              error={errors?.maNguoiThue?.message}
            />
            <Input
              label="Ngày thuê"
              register={register}
              name="ngayThue"
              placeholder="Ngày Thuê"
              error={errors?.ngayThue?.message}
            />
            <div className="mt-4">
              <Radio.Group onChange={onChange} value={genderValue}>
                <Radio value={false}>Chưa hoành thành</Radio>
                <Radio value={true}>Đã hoàn thành</Radio>
              </Radio.Group>
            </div>

            <ButtonS type="submit">ADD</ButtonS>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default AdminServiceTemplate;
const ButtonS = styled.button`
  background: #1dbf73;
  border: none;
  color: #fff;
  font-size: 16px;
  font-weight: 500;
  height: 50px;
  margin: 30px 0;
  transition: all 0.3s ease-in-out;
  width: 300px;
  &:hover {
    background: #fff !important;
    box-shadow: 2px 2px 4px 2px #1dbf73;
    color: #1dbf73 !important;
  }
  &:focus {
    outline: none;
  }
`;
