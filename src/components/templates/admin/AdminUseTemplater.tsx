import { useSelector } from "react-redux";
import { useEffect } from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { AiFillEdit, AiOutlineDelete, AiOutlineMail } from "react-icons/ai";
import { toast } from "react-toastify";
import { isAxiosError } from "axios";
import { Modal, Radio, RadioChangeEvent } from "antd";
import { styled } from "styled-components";
import { zodResolver } from "@hookform/resolvers/zod";
import { PiGenderIntersex } from "react-icons/pi";
import { FaCakeCandles } from "react-icons/fa6";
import { BsFillPersonFill, BsFillTelephoneFill } from "react-icons/bs";
import { FaLock } from "react-icons/fa";
import { SubmitHandler, useForm } from "react-hook-form";
import { useState } from "react";

import {
  Button,
  Input,
  InputAnt,
  InputPassword,
  Pagination,
} from "components/ui";
import { RootState, useAppDispatch } from "store";
import { deleteUserThunk, getAllUserThunk } from "store/quanlyAdmin/thunk";
import { UpdateAccountSchema, UpdateAccountSchemaType } from "schema";
import { quanLyAdminActions } from "store/quanlyAdmin/slice";
import { quanLyAuthServices } from "services/quanLyAuth";
import { updateUserInfoThunk } from "store/quanLyNguoiDung/thunk";

export const AdminUserTemplate = () => {
  const [genderValue, setGenderValue] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalAddUserOpen, setIsModalAddUserOpen] = useState(false);
  const [displayedProducts, setDisplayedProducts] = useState(true);
  const [chooseID, setChooseID] = useState();
  const { currentPage } = useSelector((state: RootState) => state.quanLyAdmin);
  const dispatch = useAppDispatch();

  const handlePageChange = (page: number) => {
    dispatch(quanLyAdminActions.setCurrentPage(page));
  };
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdateAccountSchemaType>({
    mode: "onChange",
    resolver: zodResolver(UpdateAccountSchema),
  });

  const onChange = (e: RadioChangeEvent) => {
    setGenderValue(e.target.value);
  };
  const showModal = (type: string) => {
    type === "addUser" ? setIsModalAddUserOpen(true) : setIsModalOpen(true);
  };

  const onSubmitAddUser: SubmitHandler<any> = async (value) => {
    value.gender = genderValue;
    value.role = "ADMIN";
    value.skill = [];
    value.certification = [];

    try {
      await quanLyAuthServices.register(value);
      toast.success("Đăng ký tài khoản thành công");
    } catch (error) {
      if (isAxiosError<{ content: string }>(error)) {
        toast.error(error.response?.data.content);
      }
    }
  };

  const onSubmit: SubmitHandler<UpdateAccountSchemaType> = async (value) => {
    value.gender = genderValue;
    try {
      dispatch(updateUserInfoThunk({ id: chooseID, value: value }));
      setIsModalOpen(false);
      toast.success("Cập nhật tài khoản thành công");
    } catch (err) {
      console.log(err);
    }
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsModalAddUserOpen(false);
  };

  const confirm = (id: string) => {
    Modal.confirm({
      title: "Do you Want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      onOk() {
        dispatch(deleteUserThunk(id))
          .unwrap()
          .then(() => {
            toast.success("Xoá thành công người dùng");
            setDisplayedProducts(!displayedProducts);
          });
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const handleSearch = (event: any) => {
    dispatch(getAllUserThunk(event.target.value));
  };

  const { allUser } = useSelector((state: RootState) => state.quanLyAdmin);
  useEffect(() => {
    dispatch(getAllUserThunk({ search: "", page: currentPage }));
  }, [displayedProducts, currentPage]);
  return (
    <div className="flex flex-col gap-5">
      <Button
        className="bg-transparent border-2 w-40 border-y-indigo-300 "
        onClick={() => showModal("addUser")}
      >
        Add new user
      </Button>
      <InputAnt
        placeholder="Tìm kiếm thông tin người dùng"
        onChange={handleSearch}
      ></InputAnt>
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Id</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>
            <th scope="col" className="px-6 py-3">
              Name
            </th>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Role</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>
            <th scope="col" className="px-6 py-3">
              Email
            </th>

            <th scope="col" className="px-6 py-3">
              skills
            </th>
            <th scope="col" className="px-6 py-3">
              Certification
            </th>
            <th scope="col" className="px-6 py-3">
              action
            </th>
          </tr>
        </thead>
        <tbody>
          {allUser &&
            allUser.map((user: any, index: number) => (
              <tr
                className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
                key={index}
              >
                <th
                  scope="row"
                  className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                >
                  {user.id}
                </th>
                <td>{user.name}</td>
                <td>{user.role}</td>
                <td>{user.email}</td>
                <td>{user.skills}</td>
                <td>{user.certification}</td>
                <td className=" items-center">
                  <button
                    className="bg-transparent"
                    onClick={() => {
                      setChooseID(user.id);
                      reset(user);
                      showModal("");
                    }}
                  >
                    <AiFillEdit className="text-2xl " />
                  </button>
                  <button
                    className="ml-8 bg-transparent"
                    onClick={() => confirm(user.id)}
                  >
                    <AiOutlineDelete className="text-2xl " />
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
      <div className="w-full  flex justify-center">
        <Pagination
          defaultCurrent={1}
          onChange={handlePageChange}
          total={500}
          className=" !my-2"
        />
      </div>

      <Modal
        open={isModalOpen}
        onCancel={handleCancel}
        footer
        width={425}
        centered
        title={<h2 className="text-center font-semibold">Update profile</h2>}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex items-center flex-col justify-center">
            <Input
              label="Email"
              disabled
              register={register}
              name="email"
              error={errors?.email?.message}
              placeholder="Your Email"
            />
            <Input
              label="Name"
              register={register}
              name="name"
              error={errors?.name?.message}
              placeholder="Your Name"
            />
            <Input
              label="Birthday"
              register={register}
              name="birthday"
              error={errors?.birthday?.message}
              placeholder="dd/mm/yy"
            />
            <div className="w-[300px]">
              <p className="mb-2 text-lg font-semibold text-start text-slate-700">
                Gender:
              </p>
              <Radio.Group onChange={onChange} value={genderValue}>
                <Radio value={false}>Male</Radio>
                <Radio value={true}>Female</Radio>
              </Radio.Group>
            </div>
            <Input
              label="Certification"
              register={register}
              name="certification"
            />
            <Input label="Skill" register={register} name="skill" />
            <ButtonS type="submit">Cập nhật</ButtonS>
          </div>
        </form>
      </Modal>
      <Modal
        open={isModalAddUserOpen}
        onCancel={handleCancel}
        footer
        width={425}
        centered
        title={<h2 className="text-center font-semibold">Add new user</h2>}
      >
        <form
          className=" w-full flex flex-col items-center "
          onSubmit={handleSubmit(onSubmitAddUser)}
        >
          <div className="mt-6 text-start flex items-center gap-2">
            <BsFillPersonFill className="text-2xl" />
            <Input
              register={register}
              name="name"
              error={errors?.name?.message}
              placeholder="Your Name"
            />
          </div>
          <div className="mt-6 text-start flex items-center gap-2">
            <AiOutlineMail className="text-2xl" />
            <Input
              register={register}
              name="email"
              error={errors?.email?.message}
              placeholder="Your Email"
            />
          </div>
          <div className="mt-6 text-start flex items-center gap-2">
            <FaLock className="text-2xl" />
            <InputPassword
              register={register}
              name="password"
              // error={errors?.password?.message}
              placeholder="Your Password"
            />
          </div>

          <div className="mt-6 text-start flex items-center gap-2">
            <BsFillTelephoneFill className="text-xl" />
            <Input
              register={register}
              name="phone"
              error={errors?.phone?.message}
              placeholder="Số điện thoại"
            />
          </div>
          <div className="mt-6 text-start flex items-center gap-2">
            <FaCakeCandles className="text-xl" />
            <Input
              register={register}
              name="birthday"
              error={errors?.birthday?.message}
              placeholder="dd/mm/yy"
            />
          </div>
          <div className="mt-6 text-start flex items-center gap-2 w-[330px]">
            <PiGenderIntersex className="text-2xl" />
            <div>
              <Radio.Group onChange={onChange} value={genderValue}>
                <Radio value={false}>Male</Radio>
                <Radio value={true}>Female</Radio>
              </Radio.Group>
            </div>
          </div>

          <div className="flex justify-center w-[350px]">
            <ButtonS type="submit">Add User</ButtonS>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default AdminUserTemplate;
const ButtonS = styled.button`
  background: #1dbf73;
  border: none;
  color: #fff;
  font-size: 16px;
  font-weight: 500;
  height: 50px;
  margin: 30px 0;
  transition: all 0.3s ease-in-out;
  width: 300px;
  &:hover {
    background: #fff !important;
    box-shadow: 2px 2px 4px 2px #1dbf73;
    color: #1dbf73 !important;
  }
  &:focus {
    outline: none;
  }
`;
