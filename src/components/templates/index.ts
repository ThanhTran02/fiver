export * from "./HomeTemplate";
export * from "./LoginTemplate";
export * from "./RegisterTemplate";
export * from "./DetailTemplate";
export * from "./JobDetailTemplate";
export * from "./ProfileTemplate";
export * from "./admin";
export * from "./ResultTemplate";
