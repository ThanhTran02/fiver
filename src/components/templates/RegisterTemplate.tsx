import { styled } from "styled-components";
import { zodResolver } from "@hookform/resolvers/zod";
import { BsFillPersonFill, BsFillTelephoneFill } from "react-icons/bs";
import { AiOutlineMail } from "react-icons/ai";
import { BiSolidKey } from "react-icons/bi";
import { FaLock } from "react-icons/fa";
import { FaCakeCandles } from "react-icons/fa6";
import { PiGenderIntersex } from "react-icons/pi";
import { useState } from "react";
import { Radio, RadioChangeEvent } from "antd";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { isAxiosError } from "axios";
import { SubmitHandler, useForm } from "react-hook-form";

import { Input, InputPassword } from "components/ui";
import { RegisterSchema, RegisterSchemaType } from "schema";
import { PATH } from "constant";
import { quanLyAuthServices } from "services/quanLyAuth";

export const RegisterTemplate = () => {
  const navigate = useNavigate();
  const [genderValue, setGenderValue] = useState(false);

  const onChange = (e: RadioChangeEvent) => {
    setGenderValue(e.target.value);
  };
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<RegisterSchemaType>({
    mode: "onChange",
    resolver: zodResolver(RegisterSchema),
  });

  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
    value.gender = genderValue;
    value.role = "ADMIN";
    value.skill = [];
    value.certification = [];
    try {
      await quanLyAuthServices.register(value);
      navigate(PATH.login);
      toast.success("Đăng ký tài khoản thành công");
    } catch (error) {
      if (isAxiosError<{ content: string }>(error)) {
        toast.error(error.response?.data.content);
      }
    }
  };
  return (
    <Container>
      <div className="grid grid-cols-2  w-full">
        <div>
          <h2 className="text-[#1dbf73] font-semibold text-4xl text-center">
            REGISTER
          </h2>
          <form
            className="pt-10 w-full flex flex-col items-center "
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="mt-6 text-start flex items-center gap-2">
              <BsFillPersonFill className="text-2xl" />
              <Input
                register={register}
                name="name"
                error={errors?.name?.message}
                placeholder="Your Name"
              />
            </div>
            <div className="mt-6 text-start flex items-center gap-2">
              <AiOutlineMail className="text-2xl" />
              <Input
                register={register}
                name="email"
                error={errors?.email?.message}
                placeholder="Your Email"
              />
            </div>
            <div className="mt-6 text-start flex items-center gap-2">
              <FaLock className="text-2xl" />
              <InputPassword
                register={register}
                name="password"
                error={errors?.password?.message}
                placeholder="Your Password"
              />
            </div>
            <div className="mt-6 text-start flex items-center gap-2">
              <BiSolidKey className="text-2xl" />
              <InputPassword
                register={register}
                name="password"
                error={errors?.password?.message}
                placeholder="Repeat your Password"
              />
            </div>

            <div className="mt-6 text-start flex items-center gap-2">
              <BsFillTelephoneFill className="text-xl" />
              <Input
                register={register}
                name="phone"
                error={errors?.phone?.message}
                placeholder="Số điện thoại"
              />
            </div>
            <div className="mt-6 text-start flex items-center gap-2">
              <FaCakeCandles className="text-xl" />
              <Input
                register={register}
                name="birthday"
                error={errors?.birthday?.message}
                placeholder="dd/mm/yy"
              />
            </div>
            <div className="mt-6 text-start flex items-center gap-2 w-[330px]">
              <PiGenderIntersex className="text-2xl" />
              <div>
                <Radio.Group onChange={onChange} value={genderValue}>
                  <Radio value={false}>Male</Radio>
                  <Radio value={true}>Female</Radio>
                </Radio.Group>
              </div>
            </div>
            <div>
              <div className="flex items-center mt-6">
                <input
                  id="link-checkbox"
                  type="checkbox"
                  value=""
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                />
                <label
                  htmlFor="link-checkbox"
                  className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                >
                  I agree all statements in{" "}
                  <a
                    href="#"
                    className="text-blue-600 dark:text-blue-500 hover:underline"
                  >
                    terms and conditions
                  </a>
                  .
                </label>
              </div>
            </div>
            <div className="flex flex-col items-start w-[350px]">
              <ButtonS>Đăng Ký</ButtonS>
              <p className=" text-gray-900">
                Bạn đã có tài khoản?{" "}
                <span
                  className="text-blue-500 cursor-pointer"
                  onClick={() => navigate(PATH.login)}
                >
                  Đăng nhập
                </span>
              </p>
            </div>
          </form>
        </div>
        <div className="w-full">
          <img
            src="https://demo5.cybersoft.edu.vn/static/media/signup.bd994738c4eb8deb2801.jpg"
            alt="logo"
            className="object-cover w-2/3"
          />
        </div>
      </div>
    </Container>
  );
};

export default RegisterTemplate;
const Container = styled.div`
  width: 100%;
`;
const ButtonS = styled.button`
  background: #1dbf73;
  border: none;
  color: #fff;
  font-size: 16px;
  font-weight: 500;
  height: 50px;
  margin: 30px 0;
  transition: all 0.3s ease-in-out;
  width: 100px;
  &:hover {
    background: #fff !important;
    box-shadow: 2px 2px 4px 2px #1dbf73;
    color: #1dbf73 !important;
  }
  &:focus {
    outline: none;
  }
`;
