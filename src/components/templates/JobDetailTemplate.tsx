import { SaoICon } from "components/icon";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { CollapseProps } from "antd";
import { toast } from "react-toastify";

import { BiRefresh } from "react-icons/bi";
import { IoIosArrowForward } from "react-icons/io";
import { AiOutlineClockCircle } from "react-icons/ai";
import { TiTick } from "react-icons/ti";

import { getCVDetailThunk } from "store/QuanLyCongViec/thunk";
import { getBinhLuanTheoCVThunk } from "store/quanLyBinhLuan/thunk";
import { styled } from "styled-components";
import { Collapse, Comment, Tabs } from "components/ui";
import { postThueCVThunk } from "store/quanLyThueCongViec/thunk";

export const JobDetailTemplate = () => {
  const params = useParams();
  const { idJob } = params;

  const { detailJob } = useSelector((state: RootState) => state.quanLyCongViec);
  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const dispatch = useAppDispatch();
  const renderSao = (sao: number | 0) => {
    const icons = [];
    for (let i = 0; i < sao; i++) {
      icons.push(<SaoICon key={i} />);
    }
    return <div className="flex gap-2">{icons}</div>;
  };

  const haldSubmitHireJob = () => {
    const currentDate = new Date();
    const day = currentDate.getDate();
    const month = currentDate.getMonth() + 1;
    const year = currentDate.getFullYear();
    const dateString = `${day}/${month}/${year}`;
    const data = {
      id: 0,
      maCongViec: detailJob?.congViec.id,
      maNguoiThue: user.id,
      hoanThanh: false,
      ngayThue: dateString,
    };

    dispatch(postThueCVThunk(data))
      .unwrap()
      .then(() => {
        toast.success("Bạn đã thuê công việc thành công");
      })
      .catch((error) => {
        toast.error(error.response.data.content);
      });
  };

  const text = `
  Voluptates amet earum velit nobis aliquam laboriosam nihil debitis facere voluptatibus consectetur quae quasi fuga, ad corrupti libero omnis sapiente non assumenda, incidunt officiis eaque iste minima autem.
`;
  const items: CollapseProps["items"] = [
    {
      key: "1",
      label: "There are many passages but the majority?",
      children: <p>{text}</p>,
    },
    {
      key: "2",
      label: "There are many passages but the majority?",
      children: <p>{text}</p>,
    },
    {
      key: "3",
      label: "There are many passages but the majority?",
      children: <p>{text}</p>,
    },
    {
      key: "4",
      label: "There are many passages but the majority?",
      children: <p>{text}</p>,
    },
  ];

  useEffect(() => {
    if (idJob) {
      dispatch(getCVDetailThunk(idJob));
      dispatch(getBinhLuanTheoCVThunk(idJob));
    }
  }, [idJob]);

  return (
    <Container>
      <div className="header w-3/5 ">
        <div className="flex items-center gap-2">
          <a href="#">{detailJob?.tenLoaiCongViec}</a>
          <IoIosArrowForward />
          <a href="#">{detailJob?.tenNhomChiTietLoai}</a>
          <IoIosArrowForward />
          <a href="#">{detailJob?.tenChiTietLoai}</a>
        </div>
        <div className="content mt-3 flex flex-col gap-4 ">
          <h3 className="font-bold text-[28px] text-[#404145]">
            {detailJob?.congViec?.tenCongViec}
          </h3>
          <div className="flex items-center text-[#62646a] gap-5">
            <img
              src={detailJob?.avatar}
              className="h-10 w-10 rounded-full"
              alt=""
            />
            <p className="text-black font-bold text-[16px] border-[#62646a]">
              {detailJob?.tenNguoiTao}
            </p>
            <p className="px-2 border-r-[1px]">
              Level {detailJob?.congViec?.saoCongViec} seller{" "}
            </p>
            {detailJob && renderSao(detailJob?.congViec?.saoCongViec)}
            <p className="border-r-[1px]">({detailJob?.congViec.danhGia})</p>
          </div>
          <div className="overflow-hidden">
            <img
              src={detailJob?.congViec.hinhAnh}
              className="object-cover w-full rounded-md animation-img"
              alt=""
            />
          </div>
          <div>
            <h3 className="font-bold text-[28px] text-[#404145]">
              About This Gig
            </h3>
            <p className="text-[#62646a] text-xl border-b-[1px] border-[#c5c6c9] pb-5">
              {detailJob?.congViec.moTa}
            </p>
          </div>
          <div>
            <h3 className="font-bold text-[28px] text-[#404145]">
              About The Seller
            </h3>
            <div className="flex gap-5">
              <div>
                <img
                  src={detailJob?.avatar}
                  className="h-28 w-28 rounded-full"
                  alt=""
                />
              </div>
              <div className="text-[#62646a] flex flex-col gap-2">
                <p className="font-bold text-xl ">{detailJob?.tenNguoiTao}</p>
                <p>{detailJob?.tenChiTietLoai}</p>
                <div className="flex gap-2 items-center">
                  {detailJob && renderSao(detailJob?.congViec?.saoCongViec)}
                  <p>({detailJob?.congViec.danhGia})</p>
                </div>
                <button className="bg-white border-[1px] outline-none border-[#62646a] px-4 py-2 rounded-md hover:border-[#62646a] hover:bg-[#74767e] duration-500 hover:text-white transition-all ">
                  Contact Me
                </button>
              </div>
            </div>
          </div>
        </div>
        <div>
          <Collapse ghost items={items} />
        </div>
        <div>
          <Comment />
        </div>
      </div>
      <div className="w-1/4 absolute  top-36 z-0 right-10">
        <Tabs
          // onChange={onChange}
          type="card"
          items={new Array(3).fill(null).map((_, i) => {
            const labelCard = ["Basic", "Standard", "Premium"];
            const id = String(i + 1);
            return {
              label: `${labelCard[i]}`,
              key: id,
              children: (
                <div className="flex flex-col gap-2">
                  <div className="font-bold text-xl flex items-center justify-between">
                    <p>{labelCard[i]}</p>
                    <p>{detailJob?.congViec.giaTien}$</p>
                  </div>
                  <div>
                    <p className="text-[#62646a]">
                      LinkedIn page US$10 LinkedIn business page create and
                      setup 3 Days Delivery Target audience research Automated
                      feed ads (DPA) Ads analytical report 3 days
                    </p>
                  </div>
                  <div className="flex items-center justify-between text-[#62646a] font-bold text-[16px]">
                    <div className="flex items-center gap-2">
                      <AiOutlineClockCircle className="text-2xl" />
                      <p>
                        14 Days <br /> Delivery
                      </p>
                    </div>
                    <div className="flex items-center  gap-2">
                      <BiRefresh className="text-2xl" />
                      <p>
                        Unlimited <br /> Revisions
                      </p>
                    </div>
                  </div>
                  <div>
                    <div className="flex  items-center  text-[15px] font-semibold text-[#62646a]   ">
                      <TiTick className="text-xl fill-[#19a463]" />
                      <p>Good fearture</p>
                    </div>
                    <div className="flex  items-center  text-[15px] font-semibold text-[#62646a]   ">
                      <TiTick className="text-xl fill-[#19a463]" />
                      <p>Good fearture</p>
                    </div>
                    <div className="flex  items-center  text-[15px] font-semibold text-[#62646a]   ">
                      <TiTick className="text-xl fill-[#19a463]" />
                      <p>Good fearture</p>
                    </div>
                  </div>
                  <div className="flex  items-center flex-col w-full">
                    <button
                      className=" text-[16px] w-2/3 border-[1px] bg-[#19a463] outline-none border-[#00b96b] px-4 py-[6px] rounded-md hover:border-[#00b96b] hover:bg-[#00b96b] duration-500 text-white transition-all my-4"
                      onClick={haldSubmitHireJob}
                    >
                      Continue ({detailJob?.congViec.giaTien}US)
                    </button>
                    <p className="text-[#19a463] font-bold text-[16px]">
                      Compare Packages
                    </p>
                  </div>
                </div>
              ),
            };
          })}
        />
      </div>
    </Container>
  );
};

export default JobDetailTemplate;
const Container = styled.div`
  position: relative;
  overflow: hidden;
  .animation-img {
    transition: all 0.5s ease;
    &:hover {
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
    }
  }
`;
