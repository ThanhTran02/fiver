import { useRef, useState } from "react";
import { Carousel } from "flowbite-react";
import { styled } from "styled-components";
import { Modal } from ".";

export const Testimonial = () => {
  const [isModalOpen, setIsModalOpen] = useState([false, false, false, false]);
  const videoRef0 = useRef<HTMLVideoElement | null>(null);
  const videoRef1 = useRef<HTMLVideoElement | null>(null);
  const videoRef2 = useRef<HTMLVideoElement | null>(null);
  const videoRef3 = useRef<HTMLVideoElement | null>(null);
  const showModal = (index: number) => {
    const updatedArray = isModalOpen.map((value, i) => {
      if (i === index) {
        return true;
      }
      return value;
    });
    setIsModalOpen(updatedArray);
  };

  const handleCancel = (index: number) => {
    const updatedArray = isModalOpen.map((value, i) => {
      if (i === index) {
        return false;
      }
      return value;
    });
    setIsModalOpen(updatedArray);

    if (videoRef0.current) {
      videoRef0.current.pause();
    }
    if (videoRef1.current) {
      videoRef1.current.pause();
    }
    if (videoRef2.current) {
      videoRef2.current.pause();
    }
    if (videoRef3.current) {
      videoRef3.current.pause();
    }
  };
  return (
    <Div>
      <Carousel className="w-full h-[300px] mt-20">
        <div className="grid grid-cols-2 gap-6 px-16 h-full w-full items-center">
          <div className="video-img " onClick={() => showModal(0)}>
            <img
              src="https://demo5.cybersoft.edu.vn/img/testimonial2.png"
              alt=".."
            />
          </div>
          <div>
            <div className="flex items-center">
              <h5 className="text-[#74767e] text-xl">
                Caitlin Tormey, Chief Commercial Officer
              </h5>
              <span className="testimonial-logo w-28 ">
                <img
                  className="img-rooted"
                  alt="Company logo"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/naadam-logo-x2.0a3b198.png"
                  loading="lazy"
                  style={{ width: "90%" }}
                />
              </span>
            </div>
            <div>
              <i className="text-3xl">
                "We've used Fiverr for Shopify web development, graphic design,
                and backend web development. Working with Fiverr makes my job a
                little easier every day."
              </i>
            </div>
          </div>
          <Modal
            open={isModalOpen[0]}
            footer
            onCancel={() => handleCancel(0)}
            className="!w-3/4"
          >
            <video className="aspect-video w-full p-3" controls ref={videoRef0}>
              <source src="./video/testimonial1.mp4" type="video/mp4" />
            </video>
          </Modal>
        </div>
        <div className="grid grid-cols-2 gap-6  px-16 h-full w-full">
          <div className="video-img " onClick={() => showModal(1)}>
            <img
              src="https://demo5.cybersoft.edu.vn/img/testimonial3.png"
              alt=".."
            />
          </div>
          <div>
            <div className="flex items-center">
              <h5 className="text-[#74767e] text-xl">
                Brighid Gannon (DNP, PMHNP-BC), Co-Founder
              </h5>
              <span className="testimonial-logo w-28">
                <img
                  className="img-rooted"
                  alt="Company logo"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lavender-logo-x2.89c5e2e.png"
                  loading="lazy"
                  style={{ width: "90%" }}
                />
              </span>
            </div>
            <div>
              <i className="text-3xl">
                "We used Fiverr for SEO, our logo, website, copy, animated
                videos — literally everything. It was like working with a human
                right next to you versus being across the world."
              </i>
            </div>
            <Modal
              open={isModalOpen[1]}
              footer
              onCancel={() => handleCancel(1)}
              className="!w-3/4"
            >
              <video
                className="aspect-video w-full p-3"
                controls
                ref={videoRef1}
              >
                <source src="./video/testimonial2.mp4" type="video/mp4" />
              </video>
            </Modal>
          </div>
        </div>
        <div className="grid grid-cols-2 gap-6  px-16 h-full w-full">
          <div className="video-img " onClick={() => showModal(2)}>
            <img
              src="https://demo5.cybersoft.edu.vn/img/testimonial4.png"
              alt=".."
            />
          </div>
          <div>
            <div className="flex items-center">
              <h5 className="text-[#74767e] text-xl">
                Tim and Dan Joo, Co-Founders
              </h5>

              <span className="testimonial-logo w-28">
                <img
                  className="img-rooted"
                  alt="Company logo"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/haerfest-logo-x2.03fa5c5.png"
                  loading="lazy"
                  style={{ width: "90%" }}
                />
              </span>
            </div>
            <div>
              <i className="text-3xl">
                "When you want to create a business bigger than yourself, you
                need a lot of help. That's what Fiverr does."
              </i>
            </div>
            <Modal
              open={isModalOpen[2]}
              footer
              onCancel={() => handleCancel(2)}
              className="!w-3/4"
            >
              <video
                className="aspect-video w-full p-3"
                controls
                ref={videoRef2}
              >
                <source src="./video/testimonial3.mp4" type="video/mp4" />
              </video>
            </Modal>
          </div>
        </div>
        <div className="grid grid-cols-2 gap-6  px-16 h-full w-full">
          <div className="video-img " onClick={() => showModal(3)}>
            <img
              src="https://demo5.cybersoft.edu.vn/img/testimonial1.png"
              alt=".."
            />
          </div>
          <div>
            <div className="flex items-center">
              <h5 className="text-[#74767e] text-xl">Kay Kim, Co-Founder</h5>
              <span className="testimonial-logo w-28 ">
                <img
                  className="img-rooted"
                  alt="Company logo"
                  src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/rooted-logo-x2.321d79d.png"
                  loading="lazy"
                  style={{ width: "90%" }}
                />
              </span>
            </div>
            <div>
              <i className="text-3xl">
                "It's extremely exciting that Fiverr has freelancers from all
                over the world — it broadens the talent pool. One of the best
                things about Fiverr is that while we're sleeping, someone's
                working."
              </i>
            </div>
            <Modal
              open={isModalOpen[3]}
              footer
              onCancel={() => handleCancel(3)}
              className="!w-3/4"
            >
              <video
                className="aspect-video w-full p-3"
                controls
                ref={videoRef3}
              >
                <source src="./video/testimonial4.mp4" type="video/mp4" />
              </video>
            </Modal>
          </div>
        </div>
      </Carousel>
    </Div>
  );
};

export default Testimonial;
const Div = styled.div`
  width: 1180px;
  margin: auto;
  .video-img {
    z-index: 0;
    position: relative;
    &::after {
      background: url(https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/desktop-play-button.c1196d6.png)
        no-repeat;
      background-size: 80px;
      content: "";
      display: inline-block;
      height: 80px;
      left: 50%;
      position: absolute;
      top: 40%;
      -webkit-transform: translateX(-50%);
      transform: translateX(-50%);
      width: 80px;
      z-index: 1;
      cursor: pointer;
    }
  }
  .testimonial-logo {
    border-left: 1px solid #c5c6c9;
    background-color: transparent;
    margin: 10px 15px;
    padding: 0 10px;
  }
  button {
    border: none;
    z-index: 1;
    background-color: none;
    padding: 0;
    border-radius: 100%;
    margin: 0;
    &:focus {
      outline: none;
    }
    &:hover {
      background-color: #616161;
    }
  }
  span {
    background-color: #b5b6ba;
    z-index: 2;
    &:hover {
      background-color: #616161;
    }
  }
`;
