import { styled } from "styled-components";

export const Container = () => {
  return (
    <ContainerS className="bg-[#fafafa] ">
      <div className="flex items-center text-[#b5b6ba] gap-4 justify-center w-[1280px] m-auto">
        <span className="text-xl font-semibold">Trusted by:</span>
        <ul className="flex  items-center gap-4">
          <li className="li">
            <img
              className="object-cover w-full h-full"
              src="https://demo5.cybersoft.edu.vn/img/fb.png"
              alt="fb"
            />
          </li>
          <li className="li">
            <img
              className="object-cover w-full h-full"
              src="https://demo5.cybersoft.edu.vn/img/google.png"
              alt="google"
            />
          </li>
          <li className="li">
            <img
              className="object-cover  h-14 w-20"
              src="https://demo5.cybersoft.edu.vn/img/netflix.png"
              alt="netflix"
            />
          </li>
          <li className="">
            <img
              src="https://demo5.cybersoft.edu.vn/img/pg.png"
              className="object-cover h-12 w-20"
              alt="pg"
            />
          </li>
          <li>
            <img
              className="object-cover w-full h-full"
              src="https://demo5.cybersoft.edu.vn/img/paypal.png"
              alt="paypal"
            />
          </li>
        </ul>
      </div>
    </ContainerS>
  );
};

export default Container;
const ContainerS = styled.section`
  max-width: 100vw !important;
  margin: auto;
  li {
    height: 50px;
    width: 100px;
  }
`;
