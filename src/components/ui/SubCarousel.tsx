import { Carousel } from "flowbite-react";
import { styled } from "styled-components";

export const SubCarousel = () => {
  return (
    <Container>
      <Carousel className="w-full h-[310px]  ">
        <div className="grid grid-cols-5 gap-2 px-16 h-full w-full  ">
          <img
            className=""
            alt="..."
            src="https://demo5.cybersoft.edu.vn/img/crs1.png"
          />

          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs2.png" />

          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs3.png" />

          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs4.png" />

          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs5.png" />
        </div>
        <div className="grid grid-cols-5 gap-2  px-16 h-full w-full">
          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs6.png" />
          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs7.png" />
          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs8.png" />
          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs9.png" />
          <img alt="..." src="https://demo5.cybersoft.edu.vn/img/crs10.png" />
        </div>
      </Carousel>
    </Container>
  );
};

export default SubCarousel;
const Container = styled.div`
  width: 1180px;
  margin: auto;
  button {
    border: none;
    z-index: 1;
    background-color: none;
    padding: 0;
    border-radius: 100%;
    margin: 0;
    &:focus {
      outline: none;
    }
    &:hover {
      background-color: #616161;
    }
  }
  span {
    background-color: #b5b6ba;
    z-index: 2;
    &:hover {
      background-color: #616161;
    }
  }
  img {
    cursor: pointer;
  }
`;
