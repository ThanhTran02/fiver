import { useEffect } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

import { Modal } from "antd";
import { ExclamationCircleFilled } from "@ant-design/icons";

import { RootState, useAppDispatch } from "store";
import { SaoICon } from "components/icon";
import { PATH } from "constant";
import {
  deleteCVHiredCVThunk,
  getCVHiredCVThunk,
} from "store/quanLyThueCongViec/thunk";
import { HireJob } from "types";

export const MyJob = () => {
  const { congViecDaThue } = useSelector(
    (state: RootState) => state.quanLyThueCongViec
  );
  let id: number;
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const haldeDelete = async (id: number) => {
    dispatch(deleteCVHiredCVThunk(id))
      .unwrap()
      .then(() => {
        dispatch(getCVHiredCVThunk());
        toast.success("Bạn đã xoá Công việc đã thành công");
      })
      .catch((error) => {
        toast.error(error.response.data.content);
      });
  };

  // ui
  const { confirm } = Modal;
  const showDeleteConfirm = () => {
    confirm({
      title: "Bạn có Muốn xoá Công việc hiện tại?",
      icon: <ExclamationCircleFilled />,
      content: "Some descriptions",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        haldeDelete(id);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  useEffect(() => {
    dispatch(getCVHiredCVThunk());
  }, []);
  return (
    <div className="w-full">
      <div className="flex justify-between border-[1px] border-[#dadbdd] py-5 px-7 rounded items-center">
        <p className="text-[#404145] font-bold text-[16px]">
          It seems that you don't have any active Gigs.
        </p>
        <button className=" text-[16px] border-[1px] bg-[#19a463] outline-none border-[#00b96b] px-4 py-[6px] rounded-md hover:border-[#00b96b] hover:bg-[#00b96b] duration-500 text-white transition-all my-4">
          Create a new Gig
        </button>
      </div>
      <div className="mt-10 ">
        {congViecDaThue?.map((cv: HireJob) => (
          <div className="grid grid-cols-3 gap-5 p-5 border-[1px] border-[#dadbdd] rounded">
            <div className="col-span-1 ">
              <img src={cv.congViec.hinhAnh} alt=".." />
            </div>
            <div className="col-span-2 flex flex-col gap-2">
              <h3 className="font-bold text-[rgba(0,0,0,.85)] text-[17px]">
                {cv.congViec.tenCongViec}
              </h3>
              <p className="text-[#616161]">{cv.congViec.moTa}</p>
              <div className="flex items-center justify-between">
                <div className="flex items-center gap-2 text-[#616161]">
                  <SaoICon />
                  <span className="font-bold text-[#ffbe5b] ">
                    {cv.congViec.saoCongViec}
                  </span>
                  <p>({cv.congViec.danhGia})</p>
                </div>
                <div className="text-[16px] font-semibold text-[#616161]">
                  <p>{cv.congViec.giaTien}$</p>
                </div>
              </div>
              <div className="flex items-center justify-around">
                <button
                  className=" text-[16px] w-1/3 border-[1px] bg-[#19a463] outline-none border-[#00b96b] px-4 py-[6px] rounded-md hover:border-[#00b96b] hover:bg-[#00b96b] duration-500 text-white transition-all my-4"
                  onClick={() => {
                    const path = generatePath(PATH.jobDetail, {
                      idJob: cv.congViec.id,
                    });
                    navigate(path);
                  }}
                >
                  View detail
                </button>
                <button
                  className=" text-[16px] w-1/3 border-[1px] bg-red-700 outline-none  px-4 py-[6px] rounded-md  hover:bg-red-900 duration-500 text-white transition-all my-4"
                  onClick={() => {
                    id = cv.id;
                    showDeleteConfirm();
                  }}
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default MyJob;
