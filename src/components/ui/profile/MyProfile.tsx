import { AiFillGithub, AiFillGoogleCircle } from "react-icons/ai";
import { GrAdd } from "react-icons/gr";
import { BsFacebook, BsFillPencilFill } from "react-icons/bs";
import { ImLocation2 } from "react-icons/im";
import { RxAvatar } from "react-icons/rx";
import { SubmitHandler, useForm } from "react-hook-form";
import { useState } from "react";
import { useSelector } from "react-redux";

import { RootState, useAppDispatch } from "store";
import { Input, Modal } from "..";
import { UpdateAccountSchema, UpdateAccountSchemaType } from "schema";
import { zodResolver } from "@hookform/resolvers/zod";
import { Radio, RadioChangeEvent } from "antd";
import {
  getUserInfoThunk,
  updateUserInfoThunk,
} from "store/quanLyNguoiDung/thunk";
import { styled } from "styled-components";
import { toast } from "react-toastify";

export const MyProfile = () => {
  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [genderValue, setGenderValue] = useState(false);
  const dispatch = useAppDispatch();
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdateAccountSchemaType>({
    mode: "onChange",
    resolver: zodResolver(UpdateAccountSchema),
  });

  const onChange = (e: RadioChangeEvent) => {
    setGenderValue(e.target.value);
  };
  const showModal = () => {
    setIsModalOpen(true);
  };

  const onSubmit: SubmitHandler<UpdateAccountSchemaType> = async (value) => {
    value.gender = genderValue;
    try {
      dispatch(updateUserInfoThunk({ id: user.id, value: value }));
      setIsModalOpen(false);
      toast.success("Cập nhật tài khoản thành công");
    } catch (err) {
      console.log(err);
    }
    dispatch(getUserInfoThunk());
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="">
      <div className="border-[1px] border-[#dadbdd]">
        <div className="card ">
          <div className="flex flex-col gap-4 items-center border-b-[1px] pb-8 border-[#dadbdd]">
            <div className="h-32 w-32 z-0 rounded-full bg-[#dadbdd]  items-center flex justify-center   ">
              {user && (
                <img
                  src={user.avatar}
                  alt=".."
                  className=" rounded-full z-10"
                />
              )}
            </div>
            <h3 className="text-[18px] font-bold text-[#222325]">
              {user && user.email}
            </h3>
            <div
              onClick={() => {
                reset(user);
                showModal();
              }}
            >
              <BsFillPencilFill className=" cursor-pointer" />
            </div>
          </div>
          <div className="text-[#62646a] mt-8 ">
            <div className="flex items-center justify-between">
              <div className="flex gap-2 items-center">
                <ImLocation2 />
                <p>From</p>
              </div>
              <p className="font-bold">Việt Nam</p>
            </div>
            <div className="flex items-center justify-between mt-4">
              <div className="flex gap-2 items-center">
                <RxAvatar />
                <p>Member since</p>
              </div>
              <p className="font-bold">Oct2022</p>
            </div>
          </div>
        </div>
      </div>
      <div className="card border-[1px] border-[#dadbdd] mt-10">
        <div className="border-b-[1px] py-8 border-[#dadbdd]">
          <div className="flex justify-between items-center">
            <h3 className="text-[18px] font-bold text-[#222325]">
              Description
            </h3>
            <div
              onClick={() => {
                reset(user);
                showModal();
              }}
            >
              <BsFillPencilFill className=" cursor-pointer" />
            </div>
          </div>
          <div className="text-[#62646a] flex flex-col gap-2 mt-2">
            <div className="flex items-center justify-between">
              <p>Name:</p>
              <span>{user && user.name}</span>
            </div>
            <div className="flex items-center justify-between">
              <p>Phone:</p>
              <span>{user?.phone}</span>
            </div>
            <div className="flex items-center justify-between">
              <p>Birthday:</p>
              <span>{user?.birthday}</span>
            </div>
          </div>
        </div>
        <div className="border-b-[1px] py-8 border-[#dadbdd] ">
          <h3 className="text-[18px] font-bold text-[#222325] mb-2">
            Languages
          </h3>
          <p className="text-[#62646a]">English</p>
          <p className="text-[#62646a]">Vietnamese</p>
        </div>
        <div className="border-b-[1px] py-8 border-[#dadbdd] ">
          <h3 className="text-[18px] font-bold text-[#222325]">Skills</h3>
        </div>
        <div className="border-b-[1px] py-8 border-[#dadbdd] ">
          <h3 className="text-[18px] font-bold text-[#222325]">Education</h3>
        </div>
        <div className="border-b-[1px] py-8 border-[#dadbdd] ">
          <h3 className="text-[18px] font-bold text-[#222325]">
            Certification
          </h3>
        </div>
        <div className="mt-8 flex flex-col gap-3">
          <h3 className="text-[18px] font-bold text-[#222325]">
            Linked Accounts
          </h3>
          <div className="flex items-center gap-2 text-[#62646a]">
            <BsFacebook />
            <p>Facebook</p>
          </div>
          <div className="flex items-center gap-2 text-[#62646a]">
            <AiFillGoogleCircle />
            <p>Google</p>
          </div>
          <div className="flex items-center gap-2 text-[#62646a]">
            <AiFillGithub />
            <p>Github</p>
          </div>
          <div className="flex items-center gap-2 text-[#62646a]">
            <GrAdd />
            <p>Dirbble</p>
          </div>
          <div className="flex items-center gap-2 text-[#62646a]">
            <GrAdd />
            <p>Stack Overflow</p>
          </div>
        </div>
      </div>
      <Modal
        open={isModalOpen}
        onCancel={handleCancel}
        footer
        width={425}
        centered
        title={<h2 className="text-center font-semibold">Update profile</h2>}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex items-center flex-col justify-center">
            <Input
              label="Email"
              disabled
              register={register}
              name="email"
              error={errors?.email?.message}
              placeholder="Your Email"
            />
            <Input
              label="Name"
              register={register}
              name="name"
              error={errors?.name?.message}
              placeholder="Your Name"
            />
            <Input
              label="Birthday"
              register={register}
              name="birthday"
              error={errors?.birthday?.message}
              placeholder="dd/mm/yy"
            />
            <div className="w-[300px]">
              <p className="mb-2 text-lg font-semibold text-start text-slate-700">
                Gender:
              </p>
              <Radio.Group onChange={onChange} value={genderValue}>
                <Radio value={false}>Male</Radio>
                <Radio value={true}>Female</Radio>
              </Radio.Group>
            </div>
            <Input
              label="Certification"
              register={register}
              name="certification"
            />
            <Input label="Skill" register={register} name="skill" />
            <ButtonS type="submit">Cập nhật</ButtonS>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default MyProfile;
const ButtonS = styled.button`
  background: #1dbf73;
  border: none;
  color: #fff;
  font-size: 16px;
  font-weight: 500;
  height: 50px;
  margin: 30px 0;
  transition: all 0.3s ease-in-out;
  width: 300px;
  &:hover {
    background: #fff !important;
    box-shadow: 2px 2px 4px 2px #1dbf73;
    color: #1dbf73 !important;
  }
  &:focus {
    outline: none;
  }
`;
