import { styled } from "styled-components";
import { Dropdown } from ".";
import { RootState } from "store";

import { useSelector } from "react-redux";
import { MenuItemType } from "antd/es/menu/hooks/useItems";
import { MenuProps } from "antd";
import { PATH } from "constant";
import { generatePath, useNavigate } from "react-router-dom";

interface DropdownOptions {
  label: React.ReactNode;
  menu?: {
    items: MenuItemType[];
  };
}

export const Navbar = () => {
  const { arrLoaiCV } = useSelector((state: RootState) => state.quanLyCongViec);
  const navigate = useNavigate();
  const onClick: MenuProps["onClick"] = ({ key }) => {
    const path = generatePath(PATH.detail, { idCV: key });
    navigate(path);
  };

  const items: DropdownOptions[] =
    arrLoaiCV?.map((cv) => ({
      label: (
        <a rel="noopener noreferrer" href="#">
          {cv.tenLoaiCongViec}
        </a>
      ),
      menu: {
        items: cv.dsNhomChiTietLoai.flatMap((item: any) =>
          item.dsChiTietLoai.map((detailItem: any) => ({
            key: detailItem.id,
            label: detailItem.tenChiTiet,
          }))
        ),
      },
    })) ?? [];

  return (
    <Container className=" flex gap-2 justify-between py-3 ">
      {items.map((item: any, i) => {
        return (
          <div className="link" key={i}>
            <Dropdown
              menu={{
                items: item?.menu?.items,
                onClick,
              }}
            >
              {item.label}
            </Dropdown>
          </div>
        );
      })}
    </Container>
  );
};

export default Navbar;
const Container = styled.div`
  max-width: 1280px;
  margin: auto;
  a {
    color: #62646a;
    &:hover {
      color: #1dbf73;
    }
  }
  .CategoriesMenu .categoriesmenu_wrapper {
    border-bottom: 1px solid #e4e5e7;
    display: block;
  }
  .CategoriesMenu .categoriesmenu_wrapper {
    background-color: #fff;
    width: 100%;
  }
  .link {
    position: relative;
    cursor: pointer;
    &::after {
      content: "";
      background: #1dbf73;
      height: 3px;
      bottom: -2px;
      left: 0;
      position: absolute;
      transition: all 0.3s;
      width: 0;
      z-index: 1;
    }
  }
  .link:hover::after {
    width: 100%;
  }
`;
