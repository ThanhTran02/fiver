import { Modal as M, ModalProps as MProps } from "antd";

export const Modal = (props: MProps) => {
  return <M {...props} />;
};

export default Modal;
