import { Switch as S, SwitchProps as SP } from "antd";

export const Switch = (props: SP) => {
  return <S {...props} />;
};
export default Switch;
