import { Radio as R, RadioProps as RP } from "antd";

export const Radio = (props: RP) => {
  return <R {...props} />;
};
Radio.Group = R.Group;
export default Radio;
