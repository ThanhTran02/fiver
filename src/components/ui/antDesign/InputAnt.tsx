import { Input as InputA, InputProps as InputPropsA } from "antd";
import { SearchProps, TextAreaProps } from "antd/es/input";
import React from "react";
type InputObject = {
  (props: InputPropsA): JSX.Element;
  Search: React.FC<SearchProps>;
  TextArea: React.FC<TextAreaProps>;
};
export const InputAnt: InputObject = (props: InputPropsA) => {
  return <InputA {...props}></InputA>;
};
InputAnt.Search = InputA.Search;
InputAnt.TextArea = InputA.TextArea;
export default InputAnt;
