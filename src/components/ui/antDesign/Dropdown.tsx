import { Dropdown as DropdownA, DropDownProps as DropDownPropsA } from "antd";

export const Dropdown = (props: DropDownPropsA) => {
  return <DropdownA {...props} />;
};
export default Dropdown;
