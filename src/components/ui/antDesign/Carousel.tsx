import { Carousel as C, CarouselProps as CP } from "antd";

export const Carousel = (props: CP) => {
  return <C {...props} />;
};

export default Carousel;
