import {
  Button as ButtonA,
  ButtonProps as ButtonPropsA,
  ConfigProvider,
} from "antd";

type ButtonProps = ButtonPropsA & {
  // định nghĩa thêm các props có thể truyền xuống
};
export const Button = (props: ButtonProps) => {
  return (
    <ConfigProvider
      theme={{
        components: {
          Button: {
            colorPrimary: "#00b96b",
            // defaultBg: "#00b96b",
            algorithm: true, // Enable algorithm
            // defaultBorderColor: "#00b96b",
          },
        },
      }}
    >
      {" "}
      <ButtonA {...props}></ButtonA>
    </ConfigProvider>
  );
};

export default Button;
