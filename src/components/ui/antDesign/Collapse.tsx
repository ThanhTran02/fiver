import { Collapse as C, CollapseProps as CP } from "antd";

export const Collapse = (props: CP) => {
  return <C {...props} />;
};

export default Collapse;
