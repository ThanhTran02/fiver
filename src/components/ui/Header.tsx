import { Button, InputAnt, Popover } from ".";
import { styled } from "styled-components";
import { useSelector } from "react-redux";
import { generatePath, useNavigate } from "react-router-dom";
import { RxAvatar } from "react-icons/rx";
import { MdManageAccounts } from "react-icons/md";

import { PATH } from "constant";
import { RootState, useAppDispatch } from "store";
import { useAuth } from "hooks";
import { EartchIcon } from "../icon";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
import { SearchProps } from "antd/es/input";

export const Header = () => {
  const { isScroll } = useSelector((state: RootState) => state.quanLyAuth);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { user } = useAuth();

  const onSearch: SearchProps["onSearch"] = async (value) => {
    if (value) {
      const path = generatePath(PATH.result, { name: value });
      navigate(path);
    }
  };

  const handleLogOut = () => {
    dispatch(quanLyNguoiDungActions.logOut());
  };
  return (
    <Container
      className={isScroll && isScroll > 100 ? `bg-white` : `bg-transparent`}
    >
      <div className="header">
        <div className="">
          <div className="flex gap-8 items-center">
            <div className="logo  flex items-center ">
              <a
                aria-current="page"
                className="logo active flex items-center"
                href="/"
              >
                <svg
                  width="89"
                  height="27"
                  viewBox="0 0 89 27"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g fill={isScroll && isScroll > 100 ? `#000` : `#fff`}>
                    <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
                  </g>
                  <g fill="#1dbf73">
                    <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
                  </g>
                </svg>
              </a>
            </div>
            <div>
              <InputAnt.Search
                placeholder="Find Services"
                allowClear
                enterButton="Search"
                size="large"
                onSearch={onSearch}
              />
            </div>
          </div>
        </div>
        {/* <div>
          <ConfigProvider
            theme={{
              components: {
                Menu: {
                  darkItemBg: "#001529",
                },
              },
            }}
          >
            <Menu
              // theme="dark"
              style={{
                backgroundColor: "transparent",
                color: "black",
                fill: "black",
              }}
              mode="horizontal"
              defaultSelectedKeys={["2"]}
              items={new Array(3).fill(null).map((_, index) => ({
                key: String(index + 1),
                label: `nav ${index + 1}`,
              }))}
            />
          </ConfigProvider>
        </div> */}
        <div
          className={
            isScroll && isScroll > 100 ? `text-[#74767e]` : `text-white`
          }
        >
          <ul className="flex gap-5 text-[17px] font-semibold my-[10px] items-center  ">
            <li>
              <p>Fiverr Business</p>
            </li>
            <li>
              <p>Explore</p>
            </li>
            <li className="flex items-center">
              <EartchIcon h={16} />
              <p>English</p>
            </li>
            <li>
              <p>US$ USD</p>
            </li>
            <li>
              <p>Become a Seller</p>
            </li>
            {user ? (
              <div className="flex gap-5">
                <Popover
                  content={
                    <div>
                      <h2 className="font-semibold p-2">{user?.name}</h2>
                      <hr />
                      <div
                        className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
                        onClick={() => navigate(PATH.profile)}
                      >
                        Thông tin tài khoản
                      </div>
                      <div
                        className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
                        onClick={handleLogOut}
                      >
                        Đăng xuất
                      </div>
                    </div>
                  }
                  trigger="click"
                >
                  <RxAvatar className="w-7 h-7" />
                </Popover>
                <div>
                  {user && user.role.replace(/\s/g, "") === "ADMIN" && (
                    <Popover
                      content={
                        <div>
                          <h2 className="font-semibold p-2">Quản trị viên</h2>
                          <hr />
                          <div
                            className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
                            onClick={() => navigate(PATH.adminUser)}
                          >
                            Đi tới trang quản lý
                          </div>
                        </div>
                      }
                    >
                      <MdManageAccounts className="w-7 h-7" />
                    </Popover>
                  )}
                </div>
              </div>
            ) : (
              <ul className="flex items-center gap-4">
                <li>
                  <p onClick={() => navigate(PATH.login)}>Sign in</p>
                </li>
                <li>
                  <Button
                    size="large"
                    style={{ fontSize: "17px" }}
                    onClick={() => navigate(PATH.register)}
                  >
                    Join
                  </Button>
                </li>
              </ul>
            )}
          </ul>
        </div>
      </div>
    </Container>
  );
};

export default Header;
const Container = styled.div`
  padding: 12px 0px;
  .header {
    display: flex;
    max-width: 1280px;
    margin: auto;
    justify-content: space-between;
    align-items: center;
  }
  .ant-input-search-button {
    background-color: #1dbf73;
    &:active {
      background-color: #19a463 !important;
    }
    &:hover {
      background-color: #19a463 !important;
    }
  }
  .css-dev-only-do-not-override-18iikkb.ant-input-affix-wrapper {
    border-color: #19a463 !important;
  }
  li {
    &:hover {
      color: #19a463;
      cursor: pointer;
    }
  }
  .ant-btn-default {
    /* background-color: #00b96b; */
    border-color: #00b96b;
    color: #1dbf73 !important;
    font-weight: 600;
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.02);
    &:hover {
      background-color: #19a463;
      color: #fff !important;
    }
  }
`;
