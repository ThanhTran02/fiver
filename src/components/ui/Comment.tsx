import { SaoICon } from "components/icon";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { InputAnt, Select } from ".";
import { CommentPostType, CommentTypes } from "types";
import { AiOutlineDislike, AiOutlineLike } from "react-icons/ai";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  getBinhLuanTheoCVThunk,
  postBinhLuanTheoCVThunk,
} from "store/quanLyBinhLuan/thunk";
import { toast } from "react-toastify";

export const Comment = () => {
  const [value, setValue] = useState("");

  const params = useParams();
  const { idJob } = params;

  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const dispatch = useAppDispatch();

  const { detailJob } = useSelector((state: RootState) => state.quanLyCongViec);

  const { arrComment } = useSelector(
    (state: RootState) => state.quanLyBinhLuan
  );
  const renderSao = (sao: number | 0) => {
    const icons = [];
    for (let i = 0; i < sao; i++) {
      icons.push(<SaoICon key={i} />);
    }
    return <div className="flex gap-2">{icons}</div>;
  };

  const handlSubmit = () => {
    if (value != "") {
      const currentDate = new Date();
      const day = currentDate.getDate();
      const month = currentDate.getMonth() + 1;
      const year = currentDate.getFullYear();
      const dateString = `${day}/${month}/${year}`;

      const comment: CommentPostType = {
        id: 0,
        maCongViec: idJob,
        maNguoiBinhLuan: user.id,
        ngayBinhLuan: dateString,
        noiDung: value,
        saoBinhLuan: 5,
      };
      dispatch(postBinhLuanTheoCVThunk(comment))
        .unwrap()
        .then(() => {
          idJob && dispatch(getBinhLuanTheoCVThunk(idJob));
        })
        .catch((error) => {
          toast.error(error.response.data.content);
        });
    } else {
      toast.error("Vui lòng điền giá trị vào mục comment");
    }
  };
  useEffect(() => {}, []);
  return (
    <div className="flex flex-col gap-5">
      <div className="header grid grid-cols-2 gap-5 text-[#404145]  ">
        <div className="left">
          <div className="content flex items-center gap-4">
            <p className="font-bold text-xl">
              {detailJob?.congViec.danhGia} Reviews
            </p>
            {detailJob && renderSao(detailJob?.congViec?.saoCongViec)}
          </div>
          <div className="flex items-center gap-2  ">
            <p className="font-semibold  text-center">5 Statrs</p>
            <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
              <div className="bg-[#ffb33e] h-2.5 rounded-full w-11/12"></div>
            </div>
            <p className="font-semibold">
              ({detailJob && detailJob?.congViec.danhGia - 1})
            </p>
          </div>
          <div className="flex items-center gap-2  ">
            <p className="font-semibold  text-center">4 Statrs</p>
            <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
              <div className="bg-[#ffb33e] h-2.5 rounded-full w-0"></div>
            </div>
            <p className="font-semibold">(0)</p>
          </div>
          <div className="flex items-center gap-2  ">
            <p className="font-semibold  text-center">3 Statrs</p>
            <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
              <div className="bg-[#ffb33e] h-2.5 rounded-full w-0"></div>
            </div>
            <p className="font-semibold">(0)</p>
          </div>
          <div className="flex items-center gap-2  ">
            <p className="font-semibold  text-center">2 Statrs</p>
            <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
              <div className="bg-[#ffb33e] h-2.5 rounded-full w-1/12"></div>
            </div>
            <p className="font-semibold">(1)</p>
          </div>
          <div className="flex items-center gap-2  ">
            <p className="font-semibold  text-center">1 Statrs</p>
            <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
              <div className="bg-[#ffb33e] h-2.5 rounded-full w-0"></div>
            </div>
            <p className="font-semibold">(0)</p>
          </div>
        </div>
        <div className="right ">
          <div className="flex gap-2 justify-end">
            <p className="text-[#62646a] font-semibold">Sort by</p>
            <Select
              defaultValue="Most Recent"
              style={{ width: 150 }}
              // onChange={handleChange}
              options={[
                { value: 0, label: "Most Recent" },
                { value: 1, label: "Most Relevant}" },
              ]}
            />{" "}
          </div>
          <p className="text-xl font-semibold mt-2">Rating Breakdown</p>
          <div className="flex items-center justify-between my-2 text-[#95979d] font-semibold text-[16px]">
            <p>Seller communication level</p>
            <div className="flex items-center gap-2 ">
              <SaoICon />
              <span className="text-[#ffb33e]">2</span>
            </div>
          </div>
          <div className="flex items-center justify-between text-[#95979d]  font-semibold text-[16px]">
            <p>Recommend to a friend</p>
            <div className="flex items-center gap-2 ">
              <SaoICon />
              <span className="text-[#ffb33e]">2</span>
            </div>
          </div>
          <div className="flex items-center justify-between my-2 text-[#95979d]  font-semibold text-[16px]">
            <p>Service as described</p>
            <div className="flex items-center gap-2 ">
              <SaoICon />
              <span className="text-[#ffb33e]">2</span>
            </div>
          </div>
        </div>
      </div>
      <div className="content">
        <div className="pb-5 border-b-[1px] border-[#c5c6c9]">
          <h3 className="font-bold text-xl text-[rgba(0,0,0,.85)] my-2">
            Filters
          </h3>
          <InputAnt.Search
            placeholder="Search reviews"
            allowClear
            enterButton="Search"
            size="large"
            //   onSearch={onSearch}
          />
        </div>
        <div className="mt-4">
          <div className="flex gap-5 py-5 border-b-[1px] border-[#c5c6c9]">
            <img
              src="https://sc04.alicdn.com/kf/Hc3e61591078043e09dba7808a6be5d21n.jpg"
              className="w-20 h-20 rounded-full object-contain"
              alt=""
            />
            <div className="text-[#404145] ">
              <div className="flex items-center gap-2 text-[#62646a]">
                <p className="font-bold text-[#404145]">idarethejeff</p>
                <SaoICon />
                <p className="text-[#ffb33e] font-bold">2</p>
              </div>
              <div className="flex items-center gap-2 my-3">
                <img
                  src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Vietnam.svg/800px-Flag_of_Vietnam.svg.png"
                  className="h-4 w-6 object-contain"
                  alt="VietNam"
                />
                <p className="font-semibold">Việt Nam</p>
              </div>
              <p className="text-[16px]">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Temporibus qui voluptatem nemo! Sit aliquam optio incidunt
                temporibus, eligendi porro ducimus nulla modi, ut deserunt
                repudiandae.
              </p>
              <div className="flex items-center gap-2 font-semibold text-[#62646a]">
                <p className="text-[#404145]  text-[16px]">Helpful?</p>
                <AiOutlineLike />
                <span>Yes</span>
                <AiOutlineDislike />
                <span>No</span>
              </div>
            </div>
          </div>
          {arrComment?.map((comment: CommentTypes) => (
            <div
              key={comment.id}
              className="flex gap-5 py-5 border-b-[1px] border-[#c5c6c9]"
            >
              {
                <img
                  src={comment.avatar}
                  className="w-20 h-20 rounded-full object-contain"
                  alt=""
                />
              }
              <div className="text-[#404145] ">
                <div className="flex items-center gap-2 my-3">
                  <p className="font-bold text-[#404145]">
                    {comment.tenNguoiBinhLuan}
                  </p>
                  <SaoICon />
                  <p className="text-[#ffb33e] font-bold">
                    {comment.saoBinhLuan}
                  </p>
                </div>
                <div className="flex items-center gap-2 my-3">
                  <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Vietnam.svg/800px-Flag_of_Vietnam.svg.png"
                    className="h-4 w-6 object-contain"
                    alt="VietNam"
                  />
                  <p className="font-semibold">Việt Nam</p>
                </div>
                <p className="text-[16px]">{comment.noiDung}</p>
                <div className="flex items-center gap-2 font-semibold text-[#62646a]">
                  <p className="text-[#404145]  text-[16px]">Helpful?</p>
                  <AiOutlineLike />
                  <span>Yes</span>
                  <AiOutlineDislike />
                  <span>No</span>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="footer">
        <div className="flex items-center justify-between my-4">
          <h3 className="text-[#404145] font-bold text-xl">
            Leave some comments
          </h3>
          <div className="flex items-center gap-4">
            {renderSao(5)}
            <p className="text-[#404145] font-bold text-xl">Rating</p>
          </div>
        </div>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            handlSubmit();
          }}
        >
          <InputAnt.TextArea
            value={value}
            onChange={(e) => setValue(e.target.value)}
            placeholder="Your comment"
            autoSize={{ minRows: 3, maxRows: 5 }}
          />
          <button className="bg-white border-[1px] outline-none border-[#00b96b] px-4 py-[6px] rounded-md hover:border-[#00b96b] hover:bg-[#00b96b] duration-500 hover:text-white transition-all my-4">
            Comment
          </button>
        </form>
      </div>
    </div>
  );
};

export default Comment;
