import { useState } from "react";
import { UseFormRegister } from "react-hook-form";
import { FaEye, FaEyeSlash } from "react-icons/fa";
type InputPasswordProps = {
  register?: UseFormRegister<any>;
  error?: string;
  name?: string;
  placeholder?: string;
};

export const InputPassword = ({
  name,
  placeholder,
  error,
  register,
}: InputPasswordProps) => {
  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  return (
    <div className="">
      <input
        type={showPassword ? "text" : "password"}
        placeholder={placeholder}
        className="border-[1px] border-[#1dbf73] text-[#212529] outline-none focus:border-[#1dbf73] focus:ring-0 rounded-[4px] px-2 py-1 w-[300px] "
        {...register?.(name || "")}
      />
      <div className=" relative bottom-[33px] left-[250px] h-0 transform -translate-y-[2px] bg-transparent border-none cursor-pointer ">
        <button
          type="button"
          className=" outline-none border-0 focus:outline-0"
          onClick={togglePasswordVisibility}
        >
          <div className="text-[16px] text-gray-500 block">
            {showPassword ? <FaEyeSlash /> : <FaEye />}
          </div>
        </button>
      </div>
      {error && <p className="text-red-500 mt-2">{error}</p>}
    </div>
  );
};

export default InputPassword;
