import { generatePath, useNavigate } from "react-router-dom";
import { styled } from "styled-components";
import { SearchProps } from "antd/es/input";

import { PATH } from "constant";
import { Carousel, InputAnt } from "components/ui";

export const Banner = () => {
  const navigate = useNavigate();
  const onSearch: SearchProps["onSearch"] = async (value) => {
    if (value) {
      const path = generatePath(PATH.result, { name: value });
      navigate(path);
    }
  };
  return (
    <Container className="w-full h-full relative ">
      <Carousel autoplay className="text-black!" dots={false}>
        <div className="banner-img ">
          <img
            src="https://demo5.cybersoft.edu.vn/img/1.png"
            className="object-cover h-full w-full"
            alt=""
          />
        </div>
        <div className="banner-img ">
          <img
            src="https://demo5.cybersoft.edu.vn/img/2.png"
            className="object-cover h-full w-full"
            alt=""
          />
        </div>
        <div className="banner-img ">
          <img
            src="https://demo5.cybersoft.edu.vn/img/3.png"
            className="object-cover  h-full w-full "
            alt=""
          />
        </div>

        <div className="banner-img">
          <img
            src="https://demo5.cybersoft.edu.vn/img/5.png"
            className="object-cover  h-full w-full"
            alt=""
          />
        </div>
        <div className="banner-img">
          <img
            src="https://demo5.cybersoft.edu.vn/img/4.png"
            className="object-cover  h-full w-full"
            alt=""
          />
        </div>
      </Carousel>

      <div className=" grid grid-cols-2 z-10 absolute top-[225px] left-16 w-[1280px] text-white">
        <div className="flex flex-col gap-5">
          <h2 className="text-5xl font-bold">
            Find the perfect freelance services for your business
          </h2>
          <InputAnt.Search
            placeholder="Try 'building mobile app'"
            allowClear
            enterButton="Search"
            size="large"
            onSearch={onSearch}
          />
          <div className="flex gap-4 font-bold items-center">
            <p>Popular:</p>
            <button className=" z-0 rounded-full bg-transparent border-[1px] border-white">
              Website Design
            </button>
            <button className=" rounded-full  bg-transparent border-[1px] border-white">
              wordPress
            </button>
            <button className="rounded-full  bg-transparent border-[1px] border-white">
              Login Design
            </button>
            <button className="rounded-full  bg-transparent border-[1px] border-white">
              Video Editing
            </button>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Banner;
const Container = styled.div`
  .ant-input-search-button {
    background-color: #1dbf73;
    font-weight: bold;
    &:active {
      background-color: #19a463 !important;
    }
    &:hover {
      background-color: #19a463 !important;
    }
  }
  .ant-btn-default {
    /* background-color: #00b96b; */
    border-color: #00b96b;
    /* background-color: black !important; */
    color: #1dbf73 !important;
    font-weight: 600;
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.02);
    &:hover {
      background-color: #19a463;
      color: #fff !important;
    }
  }
  .css-dev-only-do-not-override-18iikkb.ant-input-affix-wrapper {
    border-color: #19a463 !important;
  }
  button {
    padding: 2px 8px;
  }
  .ant-input-wrapper {
    width: 400px;
  }
  .banner-img {
    height: 100vh;
  }
`;
