export const PATH = {
  login: "/login",
  register: "/register",
  detail: "/categories/:idCV",
  jobDetail: "/jobDetail/:idJob",
  profile: "/profile",
  admin: "/admin",
  adminUser: "/admin/user",
  adminJob: "/admin/job",
  adminjobType: "/admin/jobType",
  adminService: "/admin/services",
  result: "/result/:name",
};
