import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyCongViecService, quanLyLoaiCongViecService } from "services";

export const getMenuLoaiCVThunk = createAsyncThunk(
  "quanLyCongViec/getMenuLoaiCV",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyCongViecService.getMenuLoaiCV();

      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const getDetailCVThunk = createAsyncThunk(
  "quanLyCongViec/getCongViecTheoChiTietLoai",
  async (payload: string, { rejectWithValue }) => {
    try {
      const data = await quanLyCongViecService.getCVTheoLoai(payload);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const getCVTheoTenThunk = createAsyncThunk(
  "quanLyCongViec/getCongViecTen",
  async (payload: string, { rejectWithValue }) => {
    try {
      const data = await quanLyCongViecService.getCVTheoTen(payload);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);

export const getCVDetailThunk = createAsyncThunk(
  "quanLyCongViec/getCongViecChiTiet",
  async (payload: string, { rejectWithValue }) => {
    try {
      const data = await quanLyCongViecService.getCVDetail(payload);
      return data.data.content[0];
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const updateCVThunk = createAsyncThunk(
  "quanLyCongViec/updateCongViec",
  async ({ id, value }: { id: any; value: any }, { rejectWithValue }) => {
    try {
      const data = await quanLyCongViecService.updateJobAdmin({
        id: id,
        value: value,
      });
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const deleteCVThunk = createAsyncThunk(
  "quanLyCongViec/deleteCongViec",
  async (payload: any, { rejectWithValue }) => {
    try {
      const data = await quanLyCongViecService.deleteJobAdmin(payload);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);

// loaicv
export const deleteLoaiCongViecThunk = createAsyncThunk(
  "quanLyCongViec/deleteLoaiCongViec",
  async (payload: any, { rejectWithValue }) => {
    try {
      const data = await quanLyLoaiCongViecService.deleteJobTypeAdmin(payload);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
