import { createSlice } from "@reduxjs/toolkit";
import { DetailCongViec } from "types";
import {
  getCVDetailThunk,
  getCVTheoTenThunk,
  getDetailCVThunk,
  getMenuLoaiCVThunk,
} from "./thunk";

type QuanLyCongViecInitailState = {
  arrLoaiCV?: any[];
  congViecDaThue?: [];

  arrCategory?: [];
  arrCV?: [];
  arrResult?: [];
  detailJob?: DetailCongViec;
};
const initialState: QuanLyCongViecInitailState = {};
export const quanLyCongViecSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getMenuLoaiCVThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          state.arrLoaiCV = payload;
        }
      })
      .addCase(getDetailCVThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          state.arrCategory = payload;
        }
      })
      .addCase(getCVTheoTenThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          state.arrCV = payload;
        }
      })
      .addCase(getCVDetailThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          state.detailJob = payload;
        }
      });
  },
});
export const {
  reducer: quanLyCongViecReducer,
  actions: quanLyCongViecActions,
} = quanLyCongViecSlice;
