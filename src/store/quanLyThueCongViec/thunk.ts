import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyThueCongViecService } from "services";

export const postThueCVThunk = createAsyncThunk(
  "quanLyThueCongViec/postThueCVThunk",
  async (payload: any, { rejectWithValue }) => {
    try {
      const data = await quanLyThueCongViecService.postThueCV(payload);

      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const getCVHiredCVThunk = createAsyncThunk(
  "quanLyThueCongViec/layDanhSachCongViecDaThue",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyThueCongViecService.getListCVHired();

      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const deleteCVHiredCVThunk = createAsyncThunk(
  "quanLyThueCongViec/xoaCongViecDaThue",
  async (payload: number, { rejectWithValue }) => {
    try {
      const data = await quanLyThueCongViecService.deleteJobHired(payload);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
