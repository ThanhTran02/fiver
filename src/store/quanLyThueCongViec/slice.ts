import { createSlice } from "@reduxjs/toolkit";
import { getCVHiredCVThunk } from "./thunk";
import { HireJob } from "types";

type QuanLyThueCongViecInitailState = {
  congViecDaThue?: HireJob[];
};
const initialState: QuanLyThueCongViecInitailState = { congViecDaThue: [] };
export const quanLyThueCongViecSlice = createSlice({
  name: "quanLyThueCongViec",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCVHiredCVThunk.fulfilled, (state, { payload }) => {
      if (payload) {
        state.congViecDaThue = payload;
      }
    });
  },
});
export const {
  reducer: quanLyThueCongViecReducer,
  actions: quanLyThueCongViecActions,
} = quanLyThueCongViecSlice;
