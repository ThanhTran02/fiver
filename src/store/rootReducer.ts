import { combineReducers } from "@reduxjs/toolkit";
import { quanLyAuthReducer } from "./quanLyAuth/slice";
import { quanLyNguoiDungReducer } from "./quanLyNguoiDung/slice";
import { quanLyCongViecReducer } from "./QuanLyCongViec/slice";
import { quanLyBinhLuanReducer } from "./quanLyBinhLuan/slice";
import { quanLyThueCongViecReducer } from "./quanLyThueCongViec/slice";
import { quanLyAdminReducer } from "./quanlyAdmin/slice";

export const rootReducer = combineReducers({
  quanLyAuth: quanLyAuthReducer,
  quanLyNguoiDung: quanLyNguoiDungReducer,
  quanLyCongViec: quanLyCongViecReducer,
  quanLyBinhLuan: quanLyBinhLuanReducer,
  quanLyThueCongViec: quanLyThueCongViecReducer,
  quanLyAdmin: quanLyAdminReducer,
});
