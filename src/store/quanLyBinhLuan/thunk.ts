import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyBinhLuanServices } from "services";
import { CommentPostType } from "types";

export const getBinhLuanTheoCVThunk = createAsyncThunk(
  "quanLyBinhLuan/getBinhLuanTheoCV",
  async (payload: string, { rejectWithValue }) => {
    try {
      const data = await quanLyBinhLuanServices.getBinhLuanTheoCV(payload);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const postBinhLuanTheoCVThunk = createAsyncThunk(
  "quanLyBinhLuan/postBinhLuanTheoCV",
  async (payload: CommentPostType, { rejectWithValue }) => {
    try {
      const data = await quanLyBinhLuanServices.postBinhLuan(payload);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
