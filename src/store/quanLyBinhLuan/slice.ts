import { createSlice } from "@reduxjs/toolkit";
import { getBinhLuanTheoCVThunk } from "./thunk";

type QuanLyBinhLuanInitailState = {
  arrComment?: any[];
  jobComment?: [];
};
const initialState: QuanLyBinhLuanInitailState = {};
export const quanLyBinhLuanSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getBinhLuanTheoCVThunk.fulfilled, (state, { payload }) => {
      if (payload) {
        state.arrComment = payload;
      }
    });
  },
});
export const {
  reducer: quanLyBinhLuanReducer,
  actions: quanLyBinhLuanActions,
} = quanLyBinhLuanSlice;
