import { createSlice } from "@reduxjs/toolkit";
import {
  getAllJobHiredThunk,
  getAllJobThunk,
  getAllJobTypeThunk,
  getAllUserThunk,
} from "./thunk";

type QuanLyAdminInitailState = {
  allUser?: any[];
  allServiceHire?: any[];
  allCongViec?: any[];
  allJobType?: any[];
  user?: any;
  currentPage?: number;
  // admin
};
const initialState: QuanLyAdminInitailState = {
  user: undefined,
  allUser: [],
  allCongViec: [],
  allServiceHire: [],
  allJobType: [],
  currentPage: 1,
};
export const quanLiAdminSlice = createSlice({
  name: "quanLyAdmin",
  initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllUserThunk.fulfilled, (state, { payload }) => {
        state.allUser = payload;
      })
      .addCase(getAllJobThunk.fulfilled, (state, { payload }) => {
        state.allCongViec = payload;
      })
      .addCase(getAllJobTypeThunk.fulfilled, (state, { payload }) => {
        state.allJobType = payload;
      })
      .addCase(getAllJobHiredThunk.fulfilled, (state, { payload }) => {
        state.allServiceHire = payload;
      });
  },
});
export const { reducer: quanLyAdminReducer, actions: quanLyAdminActions } =
  quanLiAdminSlice;
