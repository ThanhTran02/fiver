import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  quanLyCongViecService,
  quanLyLoaiCongViecService,
  quanLyNguoiDungServices,
  quanLyThueCongViecService,
} from "services";

export const getAllUserThunk = createAsyncThunk(
  "quanLyAdmin/getAllUserThunk",
  async (
    { search = "", page }: { search: string; page: any },
    { rejectWithValue }
  ) => {
    try {
      let data;
      if (search != "") {
        data = await quanLyNguoiDungServices.searchUserAdmin(search);
        return data.data.content;
      } else {
        const query = `/phan-trang-tim-kiem?pageIndex=${page}&pageSize=10`;
        data = await quanLyNguoiDungServices.getAllUserAdmin(query);
        return data.data.content.data;
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const deleteUserThunk = createAsyncThunk(
  "quanLyAdmin/deleteUserThunk",
  async (payload: string, { rejectWithValue }) => {
    try {
      const data = await quanLyNguoiDungServices.deleteUserAdmin(payload);

      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// job
export const getAllJobThunk = createAsyncThunk(
  "quanLyAdmin/getAllJobThunk",
  async (
    { search = "", page }: { search: string; page: any },
    { rejectWithValue }
  ) => {
    try {
      const query = `/phan-trang-tim-kiem?pageIndex=${page}&pageSize=10&keyword=${search}`;
      const data = await quanLyCongViecService.getAllJobAdmin(query);
      return data.data.content.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// jobtype
export const getAllJobTypeThunk = createAsyncThunk(
  "quanLyAdmin/getAllJobTypeThunk",
  async (
    { search = "", page }: { search: string; page: any },
    { rejectWithValue }
  ) => {
    try {
      const query = `/phan-trang-tim-kiem?pageIndex=${page}&pageSize=10&keyword=${search}`;
      const data = await quanLyLoaiCongViecService.getAllJobType(query);
      return data.data.content.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

// services
export const getAllJobHiredThunk = createAsyncThunk(
  "quanLyAdmin/getAllJobHiredThunk",
  async (
    { search = "", page }: { search: string; page: any },
    { rejectWithValue }
  ) => {
    try {
      let data;
      if (search !== "") {
        data = await quanLyThueCongViecService.searchThueCVAdmin(search);
        const arrValue = [{ ...data.data.content }];
        return arrValue;
      } else {
        const query = `/phan-trang-tim-kiem?pageIndex=${page}&pageSize=10`;
        data = await quanLyThueCongViecService.getAllThueCVAdmin(query);
        return data.data.content.data;
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
