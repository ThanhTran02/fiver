import { createAsyncThunk } from "@reduxjs/toolkit";
import { LoginSchemaType } from "schema";
import { quanLyAuthServices } from "services/quanLyAuth";

export const loginThunk = createAsyncThunk(
  "quanLyAuth/loginThunk",
  async (payload: LoginSchemaType, { rejectWithValue }) => {
    try {
      const data = await quanLyAuthServices.login(payload);
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
