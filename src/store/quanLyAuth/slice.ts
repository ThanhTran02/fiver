import { createSlice } from "@reduxjs/toolkit";
import { loginThunk } from "./thunk";

type QuanLyAuthInitailState = {
  user?: any;
  isUpdatingUser?: boolean;

  isScroll?: number;
  // admin
};
const initialState: QuanLyAuthInitailState = {
  user: undefined,
  isUpdatingUser: false,
  isScroll: 0,
};
export const quanLiAuthSlice = createSlice({
  name: "quanLyAuth",
  initialState,
  reducers: {
    logOut: (state) => {
      localStorage.removeItem("accessToken");
      state.user = undefined;
    },
    changeScroll: (state, { payload }) => {
      state.isScroll = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loginThunk.fulfilled, (state, { payload }) => {
      state.user = payload;
      if (payload) localStorage.setItem("token", payload.token);
      if (payload) localStorage.setItem("id", payload.user.id);
    });
  },
});
export const { reducer: quanLyAuthReducer, actions: quanLyAuthActions } =
  quanLiAuthSlice;
