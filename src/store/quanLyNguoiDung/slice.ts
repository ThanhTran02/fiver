import { createSlice } from "@reduxjs/toolkit";
import { getUserInfoThunk } from "./thunk";

type QuanLyNguoiDungInitailState = {
  user?: any;
  isUpdatingUser?: boolean;
};
const initialState: QuanLyNguoiDungInitailState = {
  user: undefined,
  // isUpdatingUser: false,
};
export const quanLiNguoiDungSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {
    logOut: (state) => {
      localStorage.removeItem("token");
      localStorage.removeItem("id");
      state.user = undefined;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getUserInfoThunk.fulfilled, (state, { payload }) => {
      if (payload) {
        state.user = payload;
      }
    });
  },
});
export const {
  reducer: quanLyNguoiDungReducer,
  actions: quanLyNguoiDungActions,
} = quanLiNguoiDungSlice;
