import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyNguoiDungServices } from "services";

export const getUserInfoThunk = createAsyncThunk(
  "quanLyNguoiDung/getUserInfo",

  async (_, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("token");

      if (accessToken) {
        const id = localStorage.getItem("id");
        const data = await quanLyNguoiDungServices.getUserInfo(id);
        return data.data.content;
      }
      return undefined;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const updateUserInfoThunk = createAsyncThunk(
  "quanLyNguoiDung/updateUserInfo",
  async ({ id, value }: { id: any; value: any }, { rejectWithValue }) => {
    try {
      const data = await quanLyNguoiDungServices.updateUserInfo({
        id: id,
        value: value,
      });

      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
