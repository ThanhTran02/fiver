export interface Job {
  id: number;
  tenChiTietLoai: string;
  tenNhomChiTietLoai: string;
  tenNguoiTao: string;
  tenLoaiCongViec: string;
  avatar: string;
}

export interface CongViec {
  id: any;
  danhGia: number;
  giaTien: number;
  maChiTietLoaiCongViec: number;
  moTa: string;
  moTaNgan: string;
  nguoiTao: number;
  saoCongViec: number;
  tenCongViec: string;
  hinhAnh: string;
}

export interface DetailCongViec {
  id: number;
  avatar: string;
  congViec: CongViec;
  tenChiTietLoai: string;
  tenLoaiCongViec: string;
  tenNguoiTao: string;
  tenNhomChiTietLoai: string;
}
export interface HireJob {
  id: number;
  congViec: CongViec;
  maNguoiThue: number;
  ngayThue: string;
  hoanThanh: boolean;
}
