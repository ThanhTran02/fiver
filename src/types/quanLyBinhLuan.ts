export type Cinema = {
  maHeThongRap: string;
  tenHeThongRap: string;
  biDanh: string;
  logo: string;
};
export type MovieCinema = {
  maRap: number;
  tenRap: string;
};
export type CommentTypes = {
  id: number;
  ngayBinhLuan: string;
  noiDung: string;
  saoBinhLuan: number;
  tenNguoiBinhLuan: string;
  avatar: string;
};
export type CommentPostType = {
  id: number;
  ngayBinhLuan: string;
  noiDung: string;
  saoBinhLuan: number;
  maNguoiBinhLuan: number;
  maCongViec: any;
};
