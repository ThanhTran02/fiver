import { z } from "zod";
export const LoaiCVSchema = z.object({
  id: z.any(),
  tenLoaiCongViec: z.string().nonempty("Không được để trống giá trị này"),
});
export type LoaiCVSchemaType = z.infer<typeof LoaiCVSchema>;
