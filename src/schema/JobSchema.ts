import { z } from "zod";
export const JobSchema = z.object({
  id: z.any(),
  danhGia: z.any(),
  giaTien: z.any(),
  maChiTietLoaiCongViec: z.any(),
  moTa: z.string().nonempty("Vui lòng nhập giá trị"),
  moTaNgan: z.any(),
  nguoiTao: z.any(),
  saoCongViec: z.any(),
  tenCongViec: z.any(),
  hinhAnh: z.any(),
});
export type JobSchemaType = z.infer<typeof JobSchema>;
