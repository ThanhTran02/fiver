import { z } from "zod";

export const RegisterSchema = z.object({
  name: z.string().nonempty("Vui lòng nhập tên người dùng"),
  email: z
    .string()
    .nonempty("Vui lòng nhập email")
    .email("Vui Lòng nhập đúng email"),
  password: z.string().nonempty("Vui lòng nhập mật khẩu"),
  phone: z.string().nonempty("Vui lòng nhập số điện thoại"),
  birthday: z.string().nonempty("Vui lòng nhập ngày sinh"),
  gender: z.any(),
  role: z.any(),
  skill: z.any(),
  certification: z.any(),
});
export type RegisterSchemaType = z.infer<typeof RegisterSchema>;
