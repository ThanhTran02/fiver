import { z } from "zod";
export const JobHiredSchema = z.object({
  id: z.any(),
  maCongViec: z.string().nonempty("Vui lòng nhập giá trị"),
  maNguoiThue: z.string().nonempty("Vui lòng nhập giá trị"),
  ngayThue: z.string().nonempty("Vui lòng nhập giá trị"),
  hoanThanh: z.string().nonempty("Vui lòng nhập giá trị"),
});
export type JobHiredSchemaType = z.infer<typeof JobHiredSchema>;
