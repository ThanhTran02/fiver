import { JobDetailTemplate } from "components/templates";

export const JobDetail = () => {
  return <JobDetailTemplate />;
};

export default JobDetail;
