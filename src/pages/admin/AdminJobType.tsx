import { AdminJobTypeTemplate } from "components/templates";

export const AdminJobType = () => {
  return <AdminJobTypeTemplate />;
};

export default AdminJobType;
