import { ProfileTemplate } from "components/templates";

export const Profile = () => {
  return <ProfileTemplate />;
};

export default Profile;
