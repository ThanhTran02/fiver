export * from "./Home";
export * from "./Login";
export * from "./Register";
export * from "./Detail";
export * from "./JobDetail";
export * from "./Profile";
export * from "./admin";
export * from "./Result";
