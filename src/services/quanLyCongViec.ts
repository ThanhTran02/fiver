import { apiInstance } from "constant";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_CONG_VIEC_API,
});
export const quanLyCongViecService = {
  getMenuLoaiCV: () => api.get<ApiResponse<any[]>>(`/lay-menu-loai-cong-viec`),
  getCVTheoLoai: (id: string) =>
    api.get<ApiResponse<any>>(`/lay-cong-viec-theo-chi-tiet-loai/${id}`),
  getCVDetail: (id: string) =>
    api.get<ApiResponse<any>>(`/lay-cong-viec-chi-tiet/${id}`),
  getCVTheoTen: (name: string) =>
    api.get<ApiResponse<any>>(`/lay-danh-sach-cong-viec-theo-ten/${name}`),
  // admin
  getAllJobAdmin: (query: string) => api.get<ApiResponse<any>>(`${query}`),

  addJobAdmin: (payload: any) => {
    api.post<ApiResponse<any>>("", payload);
  },

  updateJobAdmin: ({ id, value }: { id: number; value: any }) =>
    api.put<ApiResponse<any>>(`/${id}`, value),
  deleteJobAdmin: (id: any) => api.delete<ApiResponse<any>>(`/${id}`),
};
