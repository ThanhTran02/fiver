import { apiInstance } from "constant";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_LOAI_CONG_VIEC_API,
});
export const quanLyLoaiCongViecService = {
  getAllJobType: (query: string) => api.get<ApiResponse<any>>(`${query}`),
  deleteJobTypeAdmin: (id: string) => api.delete<ApiResponse<any>>(`${id}`),
  addJobTypeAdmin: (payload: any) => api.post<ApiResponse<any>>("", payload),
  updateJobTypeAdmin: ({ id, value }: { id: any; value: any }) =>
    api.put<ApiResponse<any>>(`/${id}`, value),
};
