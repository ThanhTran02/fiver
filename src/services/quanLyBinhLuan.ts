import { apiInstance } from "constant";
import { CommentPostType } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_BINH_LUAN_API,
});
export const quanLyBinhLuanServices = {
  getBinhLuanTheoCV: (payload: any) =>
    api.get<ApiResponse<any>>(`/lay-binh-luan-theo-cong-viec/${payload}`),
  postBinhLuan: (payload: CommentPostType) =>
    api.post<ApiResponse<any>>("", payload),
};
