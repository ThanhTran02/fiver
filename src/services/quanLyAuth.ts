import { apiInstance } from "constant";
import { LoginSchemaType, RegisterSchemaType } from "schema";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_AUTH_API,
});
export const quanLyAuthServices = {
  login: (payload: LoginSchemaType) =>
    api.post<ApiResponse<any>>("/signin", payload),
  register: (payload: RegisterSchemaType) => api.post("/signup", payload),
};
