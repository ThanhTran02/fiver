import { apiInstance } from "constant";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_THUE_CONG_VIEC_API,
});
export const quanLyThueCongViecService = {
  postThueCV: (payload: any) => api.post<ApiResponse<any>>("", payload),
  getListCVHired: () => api.get<ApiResponse<any[]>>("/lay-danh-sach-da-thue"),
  deleteJobHired: (id: number) => api.delete<ApiResponse<any>>(`/${id}`),

  // admin
  getAllThueCVAdmin: (query: string) => api.get<ApiResponse<any>>(`${query}`),
  searchThueCVAdmin: (id: string) => api.get<ApiResponse<any>>(`${id}`),
  addThueCVAdmin: (payload: any) => api.post<ApiResponse<any>>("", payload),
  updateThueCVAdmin: ({ id, value }: { id: any; value: any }) =>
    api.put<ApiResponse<any>>(`/${id}`, value),
};
