import { apiInstance } from "constant";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API,
});
export const quanLyNguoiDungServices = {
  getUserInfo: (query: any) => api.get<ApiResponse<any>>(`/${query}`),
  updateUserInfo: ({ id, value }: { id: number; value: any }) =>
    api.put<ApiResponse<any>>(`/${id}`, value),

  // admin

  getAllUserAdmin: (query: string) => api.get<ApiResponse<any>>(`${query}`),
  deleteUserAdmin: (id: string) => api.delete<ApiResponse<any[]>>(`?id=${id}`),
  searchUserAdmin: (payload: string) =>
    api.get<ApiResponse<any[]>>(`/search/${payload}`),
};
